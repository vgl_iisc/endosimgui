/*
File : main.cpp
Author : Tarun Bansal
Description : Functions for development of graphical interface
*/

//#define INTEGRATION

#ifdef INTEGRATION
	#include <boost/interprocess/windows_shared_memory.hpp>
	#include <boost/interprocess/mapped_region.hpp>
#endif

#include "mod_utils.h"
#include "my_vector.h"
#include "trackball.h"

#define WINDOW_TITLE_PREFIX "ME PROJECT"
#define BUFFER_OFFSET(i) ((char *)NULL + (i))
#define TRUE 1
#define FALSE 0
#define RENORMCOUNT 97

#ifdef INTEGRATION
	using namespace boost::interprocess;
	int *myint;
#endif

char texture_path[] = "data_items/textures/tissue/tissue.bmp", texture_tmp_path[] = "data_items/textures/tissue/lap.bmp";
char reflectance_map_path[] = "data_items/textures/normal_map/normal_map_perlin_noise.bmp", reflectance_map_tmp_path[] = "data_items/textures/normal_map/perlin_normal.bmp";
char gloss_map_path[] = "data_items/textures/gloss_map/gloss_map2.bmp", gloss_map_tmp_path[] = "data_items/textures/gloss_map/gloss_map.bmp";
char mucous_path[] = "data_items/textures/mucous/mucous2.bmp", mucous_tmp_path[] = "data_items/textures/mucous/mucous1.bmp";

GLint is_point_light, is_normal_mapping, is_gloss_mapping, is_invert_narmal, is_internal, is_diffuse, is_specular, is_mucous, show_patch, is_wireframe, is_partial_blending;
GLint which_texture, which_ref_map, which_gloss_map, which_mucous;
GLuint texture_uloc, texture_tmp_uloc, ref_map_uloc, ref_map_tmp_uloc, gloss_map_uloc, gloss_map_tmp_uloc, mucous_uloc, mucous_tmp_uloc;
GLuint is_diffuse_uloc, is_specular_uloc, is_point_light_uloc, is_normal_mapping_uloc, is_gloss_mapping_uloc, is_invert_narmal_uloc, is_mucous_uloc, show_patch_uloc, is_partial_blending_uloc;
GLuint model_mat_uloc, normal_mat_uloc, proj_mat_uloc;
GLuint dir_light_dir_uloc, light_pos_uloc;
GLuint cons_atten_uloc, lin_atten_uloc, qd_atten_uloc;
GLuint texture_scalling_uloc, light_col_uloc, amb_light_col_uloc, shiny_uloc, partial_blending_uloc;
GLuint which_texture_uloc, which_ref_map_uloc, which_gloss_map_uloc, which_mucous_uloc;
GLuint tex_id, ref_map_id, gloss_map_id, mucous_id;
GLuint buff_ids[6] = { 0 }, shad_ids[3] = { 0 };
GLuint num_triangle_circle = 80;
GLfloat modelview_matrix[16];
GLfloat modelview_matrix_tmp[16];

GLfloat *pixel;
int show_normals = 0;

float dir_light_dir[3] = { 0.0f, 0.0f, 1.0f };  // directional source light direction
float light_col[3] = { 1.0f, 1.0f, 1.0f };
float amb_light_col[3] = { 0.1f, 0.1f, 0.1f };
float cons_atten = 1.0f, lin_atten = 0.00f, qd_atten = 0.0f;
int shiny = 50;
float texture_scalling = 1.0f;
float partial_blending = 1.0f;

cam_path lWalkthrough;
BSpline smoothtube;
int cam_pos = 0, cam_pos_e = 0;

int cur_width = 600, cur_height = 600, win_handle = 0;
unsigned frame_cnt = 0;
unsigned int num_triangle;
Vector3f light_pos;

Camera3d freeCam, activeCamera;
float camS;				// camera speed
float camM = 1;				// camera zoom
float camT, camP;       // camera theta and phi
float aspectRatio;

int MOUSEX = cur_width / 2;
int MOUSEY = cur_height / 2;
int isRender = 0;
int isMouse = 0;
int show_tube = 0;
Vector2f old;
static float TRACKBALL_QUAR[4] = { 0., 0., 0., 1. };
//static float TRACKBALL_QUAR[4] = { 0.7071067811865, 0., 0., 0.7071067811865 };

model mod;

int gpu = 1;

enum { col1, col2, dep, cnt };
GLuint fb, rb[cnt];

typedef struct tri_normals{
	float normal[3];
}tri_normals;

typedef struct normals{
	tri_normals *tns;
}normals;

normals nor;

GLUquadricObj *IDquadric;

void processTrackBall(const Vector2f& old, const Vector2f& now){
	float q[4], tmp[4];
	trackball(q, old.x, old.y, now.x, now.y);
	//float t = q[1];
	//q[1] = q[2];
	//q[2] = t;

	tmp[0] = TRACKBALL_QUAR[0];
	tmp[1] = TRACKBALL_QUAR[1];
	tmp[2] = TRACKBALL_QUAR[2];
	tmp[3] = TRACKBALL_QUAR[3];
	add_quats(tmp, q, TRACKBALL_QUAR);

	/*
	TRACKBALL_QUAR[0] = q[0];
	TRACKBALL_QUAR[1] = q[1];
	TRACKBALL_QUAR[2] = q[2];
	TRACKBALL_QUAR[3] = q[3];
	*/

	//normalize_quat( TRACKBALL_QUAR);
	//char title[100];
	//sprintf(title,"%f, %f, %f, %f", TRACKBALL_QUAR[0],TRACKBALL_QUAR[1],TRACKBALL_QUAR[2],TRACKBALL_QUAR[3]);
	//glutSetWindowTitle(title);
}

void mousefunc(int button, int state, int a, int b){
	MOUSEX = a; MOUSEY = b;
	float x = -0.5 * (float)(2.0*a - cur_width) / (float)(cur_width);
	float y = 0.5 *(float)(2.0*b - cur_height) / (float)(cur_height);
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		old.set(x, y);

		isRender = 1;
	}
	else if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		isRender = 0;


	}
}

void reset_quat(){
	TRACKBALL_QUAR[0] = 0;
	TRACKBALL_QUAR[1] = 0.;
	TRACKBALL_QUAR[2] = 0.;
	TRACKBALL_QUAR[3] = 1;
	isMouse = 0;
}

int theta = 0;

void mousemotionfunc(int a, int b){

	theta++;
	isMouse = 1;
	MOUSEX = a; MOUSEY = b;
	float x = -0.5 *(float)(2.0*a - cur_width) / (float)(cur_width);
	float y = 0.5 *(float)(2.0*b - cur_height) / (float)(cur_height);
	Vector2f now(x, y);

	float dX = x - old.x;
	float dY = y - old.y;
	camP += dX*.5;
	camT -= dY*.5;

	processTrackBall(old, now);
	old = now;
	isMouse = 1;
	isRender = 1;
}

void mousepass(int x, int y){
	MOUSEX = x; MOUSEY = y;
}

void mouseWheel(int button, int dir, int x, int y)
{
	if (dir > 0)
	{
		if (camM> .2)
			camM = camM - .1;// Zoom in
	}
	else
	{
		if (camM < 1.0)
			camM = camM + .1;// Zoom out
	}

	glUseProgram(shad_ids[0]);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	Camera3d activeCamera = freeCam;
	gluPerspective(30 * camM, 1, 0.1, 1000);
	exit_on_gl_error("ERROR: 1.Could not use the shader program");
	GLfloat matrix[16];
	glGetFloatv(GL_PROJECTION_MATRIX, matrix);
	GLfloat pmatrix[16];
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			//pmatrix[4 * j + i] = matrix[4 * i + j];
			pmatrix[4 * i + j] = matrix[4 * i + j];
		}
	}
	glUniformMatrix4fv(proj_mat_uloc, 1, GL_FALSE, pmatrix);
	glUseProgram(0);
	return;
}

void keyboard(unsigned char Key, int X, int Y)
{
	float cam_movement = 0.01f;
	float col_step = 0.1f;
	float const_atten_step = .01f;
	float lin_atten_step = .01f;
	float qd_atten_step = .001f;
	float texture_scalling_step = .01f;
	float partial_blending_step = .1f;
	switch (Key)
	{
	case 'x':
	{

		freeCam.eye.x += cam_movement;
		freeCam.pos.x += cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3] + cam_movement, lWalkthrough.pts[3 * cam_pos + 4], lWalkthrough.pts[3 * cam_pos + 5]);
		light_pos.x = freeCam.eye.x;
		break;
	}
	case 'X':
	{

		freeCam.eye.x -= cam_movement;
		freeCam.pos.x -= cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3] - cam_movement, lWalkthrough.pts[3 * cam_pos + 4], lWalkthrough.pts[3 * cam_pos + 5]);
		light_pos.x = freeCam.eye.x;
		break;
	}
	case 'y':
	{
		freeCam.eye.y += cam_movement;
		freeCam.pos.y += cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3], lWalkthrough.pts[3 * cam_pos + 4] + cam_movement, lWalkthrough.pts[3 * cam_pos + 5]);
		light_pos.y = freeCam.eye.y;
		break;
	}
	case 'Y':
	{
		freeCam.eye.y -= cam_movement;
		freeCam.pos.y -= cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3], lWalkthrough.pts[3 * cam_pos + 4] - cam_movement, lWalkthrough.pts[3 * cam_pos + 5]);
		light_pos.y = freeCam.eye.y;
		break;
	}
	case 'z':
	{
		freeCam.eye.z += cam_movement;
		freeCam.pos.z += cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3], lWalkthrough.pts[3 * cam_pos + 4], lWalkthrough.pts[3 * cam_pos + 5] + cam_movement);
		light_pos.z = freeCam.eye.z;
		break;
	}
	case 'Z':
	{
		freeCam.eye.z -= cam_movement;
		freeCam.pos.z -= cam_movement;
		//freeCam.eye.set(lWalkthrough.pts[3 * cam_pos + 3], lWalkthrough.pts[3 * cam_pos + 4], lWalkthrough.pts[3 * cam_pos + 5] - cam_movement);
		light_pos.z = freeCam.eye.z;
		break;
	}
	case 'n': case 'N':
	{
		is_normal_mapping = (is_normal_mapping + 1) % 2;
		break;
	}
	case 'g': case 'G':
	{
		is_gloss_mapping = (is_gloss_mapping + 1) % 2;
		break;
	}
	case 'Q':
	case 'q':
	{
		is_mucous = (is_mucous + 1) % 2;
		break;
	}

	case '`':
	{
		show_patch = (show_patch + 1) % 2;
		break;
	}

	case '@':
	{
		is_partial_blending = (is_partial_blending + 1) % 2;
		break;
	}

	case 'w':
	case 'W':
	{
		is_wireframe = (is_wireframe + 1) % 2;
		break;
	}

	case '#':
	{
		show_normals = (show_normals + 1) % 2;
		break;
	}

	case  033: // Escape Key
	{
		exit(EXIT_SUCCESS);
		break;
	}

	case 'd': case 'D':
	{
		is_diffuse = (is_diffuse + 1) % 2;
		break;
	}

	case 's': case 'S':
	{
		is_specular = (is_specular + 1) % 2;
		break;
	}


	case 'i': case 'I':
	{
		is_invert_narmal = (is_invert_narmal + 1) % 2;
		break;
	}

	case 'p': case 'P':
	{
		is_point_light = (is_point_light + 1) % 2;
		//light_pos[0] = freeCam.pos.x; light_pos[1] = freeCam.pos.y; light_pos[2] = freeCam.pos.z;
		//float light_pos[3]      = {-lWalkthrough.pts[3*cam_pos + 0], -lWalkthrough.pts[3*cam_pos + 1], -lWalkthrough.pts[3*cam_pos + 2]};
		break;
	}
	case 'a':
	{
		isMouse = 0;
		show_tube = 1;
		cam_pos = (cam_pos + 1) % lWalkthrough.num_pos;
		cam_pos_e = cam_pos;
		freeCam.pos = lWalkthrough.pts[cam_pos];
		camT = -.082869f;
		camP = -5.947087f;
		freeCam.ar = aspectRatio;
		freeCam.eye = lWalkthrough.pts[cam_pos + 1];
		int temp_cam_pos = cam_pos + 8;
		light_pos = lWalkthrough.pts[temp_cam_pos];
		reset_quat();
		break;
	}
	case 'A':
	{
		isMouse = 0;
		cam_pos = (cam_pos - 1) % lWalkthrough.num_pos;
		cam_pos_e = cam_pos;
		freeCam.pos = lWalkthrough.pts[cam_pos];
		camT = -.082869f;
		camP = -5.947087f;
		freeCam.ar = aspectRatio;
		freeCam.eye = lWalkthrough.pts[cam_pos + 1];
		int temp_cam_pos = cam_pos + 8;
		light_pos = lWalkthrough.pts[temp_cam_pos];
		reset_quat();
		break;
	}
	case 'm': case 'M':
	{
		is_internal = (is_internal + 1) % 2;
		is_invert_narmal = (is_invert_narmal + 1) % 2;
		cam_pos = 0;
		if (is_internal)
		{
			freeCam.pos.set(-2.011703f, 13.954667f, -52.099850f);
			camT = -.082869f;
			camP = -5.947087f;
			freeCam.ar = aspectRatio;
			freeCam.eye = freeCam.pos + Vector3f(sin(camP)*cos(camT), sin(camT), cos(camP)*cos(camT));
			//freeCam.eye.set(-1.584985 , 14.045818, -51.099815);
			light_pos = freeCam.pos;
		}
		else
		{
			freeCam.pos.set(0, 0, 40);
			camT = -.082869f;
			camP = -5.947087f;
			freeCam.ar = aspectRatio;
			freeCam.eye.set(0, 0, 0);
			light_pos = freeCam.pos;
		}

		break;
	}

	case 'e':
	{
		cam_pos_e = (cam_pos_e + 1) % lWalkthrough.num_pos;
		light_pos = lWalkthrough.pts[cam_pos_e + 4];
		break;
	}
	case 'E':
	{
		cam_pos_e = (cam_pos_e - 1) % lWalkthrough.num_pos;
		light_pos = lWalkthrough.pts[cam_pos_e + 4];
		break;
	}

	case 'r': case 'R':
	{
		which_texture = (which_texture + 1) % 2;
		break;
	}
	case 'f': case 'F':
	{
		which_gloss_map = (which_gloss_map + 1) % 2;
		break;
	}
	case 'b': case 'B':
	{
		which_ref_map = (which_ref_map + 1) % 2;
		break;
	}

	case ':': case ';':
	{
		which_mucous = (which_mucous + 1) % 2;
		break;
	}

	case '1':
	{
		light_col[0] += col_step;
		break;
	}
	case '2':
	{
		light_col[0] -= col_step;
		break;
	}
	case '3':
	{
		light_col[1] += col_step;
		break;
	}
	case '4':
	{
		light_col[1] -= col_step;
		break;
	}

	case '5':
	{
		light_col[2] += col_step;
		break;
	}
	case '6':
	{
		light_col[2] -= col_step;
		break;
	}

	case '7':
	{
		shiny += 1;
		break;
	}

	case '8':
	{
		shiny -= 1;
		break;
	}

	case 'c':
	{
		cons_atten += const_atten_step;
		break;
	}
	case 'C':
	{
		cons_atten -= const_atten_step;
		break;
	}

	case 'l':
	{
		lin_atten += lin_atten_step;
		break;
	}
	case 'L':
	{
		lin_atten -= lin_atten_step;
		break;
	}

	case 'o':
	{
		qd_atten += qd_atten_step;
		break;
	}
	case 'O':
	{
		qd_atten -= qd_atten_step;
		break;
	}

	case 't':
	{
		texture_scalling += texture_scalling_step;
		break;
	}
	case 'T':
	{
		texture_scalling -= texture_scalling_step;
		break;
	}

	case '+':
	{
		partial_blending += partial_blending_step;
		break;
	}
	case '=':
	{
		partial_blending -= partial_blending_step;
		break;
	}

	case '!':
	{
		gpu = (gpu + 1) % 2;
		break;
	}

	default:
		break;

	}

	isRender = 1;

	fprintf(stderr, "\n\n------Printing details starts---- ");
	fprintf(stderr, "\nis_gloss_map = %d, is_normal_map = %d, is_mucous = %d, is_point_light = %d, is_normal_inverted = %d", is_gloss_mapping, is_normal_mapping, is_mucous, is_point_light, is_invert_narmal);
	fprintf(stderr, "\nlight_r = %f, light_g = %f, light_b = %f", light_col[0], light_col[1], light_col[2]);
	fprintf(stderr, "\ncons_atten = %f, lin_atten = %f, qd_atten = %f", cons_atten, lin_atten, qd_atten);
	fprintf(stderr, "\ntexture_scalling = %f", texture_scalling);
	fprintf(stderr, "\partial_blending = %f", partial_blending);
	fprintf(stderr, "\nshiny = %d", shiny);
	fprintf(stderr, "\ntexture  = %s", (which_texture == 0 ? texture_path : texture_tmp_path));
	fprintf(stderr, "\mucous  = %s", (which_mucous == 0 ? mucous_path : mucous_tmp_path));
	fprintf(stderr, "\nreflectance map = %s", (which_ref_map == 0 ? reflectance_map_path : reflectance_map_tmp_path));
	fprintf(stderr, "\ngloss map = %s", (which_gloss_map == 0 ? gloss_map_path : gloss_map_tmp_path));
	fprintf(stderr, "\ncam_pos = %f", freeCam.pos.z);
	fprintf(stderr, "\n------Printing details ends---- ");

}

void draw_model(void)
{
	glUseProgram(shad_ids[0]);
	glClearColor(0.0, 0.0, 0.0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glStencilFunc(GL_EQUAL, 0x1, 0x1);
	//glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	// added code starts
	/* glEnable(GL_STENCIL_TEST);
	glClear(GL_STENCIL_BUFFER_BIT);
	glStencilFunc(GL_ALWAYS, 1, 1);
	glColorMask(false, false, false, false);
	glDepthMask(false);
	glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glBindVertexArray(buff_ids[1]);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawElements(GL_TRIANGLES, num_triangle_circle * 3, GL_UNSIGNED_INT, (GLvoid*)0);
	glBindVertexArray(0);
	glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
	glStencilFunc(GL_EQUAL, 1, 1);
	glColorMask(true, true, true, true);
	glDepthMask(true);
	*/
	activeCamera = freeCam;
	exit_on_gl_error("ERROR: 13.Could not use the shader program");

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	Vector3f old_light_pos = light_pos;


	if (isMouse == 1){
		float mat[4 * 4], tmat[4][4];
		//glTranslatef( 0., -50., -250.);
		//glTranslatef( -activeCamera.pos.x, -activeCamera.pos.y, -activeCamera.pos.z);
		//glTranslatef(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
		build_rotmatrix(tmat, TRACKBALL_QUAR);
		for (int i = 0; i <= 3; i++){
			for (int j = 0; j <= 3; j++){
				mat[4 * j + i] = tmat[i][j];
			}
		}
		glMultMatrixf((GLfloat*)mat);
		//glTranslatef(TRACKBALL_QUAR[2], 0, 0);
	}

	/*if (isMouse == 1){

	freeCam.eye = freeCam.pos + Vector3f(sin(camP)*cos(camT), sin(camT), cos(camP)*cos(camT));
	activeCamera = freeCam;

	gluLookAt(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z,
	activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z,
	activeCamera.ori.x, activeCamera.ori.y, activeCamera.ori.z);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix_tmp);

	//light_pos[0] = modelview_matrix[0] * old_light_pos[0] + modelview_matrix[4] * old_light_pos[1] + modelview_matrix[8] * old_light_pos[2] + modelview_matrix[12];
	//light_pos[1] = modelview_matrix[1] * old_light_pos[0] + modelview_matrix[5] * old_light_pos[1] + modelview_matrix[9] * old_light_pos[2] + modelview_matrix[13];
	//light_pos[2] = modelview_matrix[2] * old_light_pos[0] + modelview_matrix[6] * old_light_pos[1] + modelview_matrix[10] * old_light_pos[2] + modelview_matrix[14];

	}
	else{
	gluLookAt(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z,
	activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z,
	activeCamera.ori.x, activeCamera.ori.y, activeCamera.ori.z);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix_tmp);

	}
	*/

#ifdef INTEGRATION
	float mark = (*myint) / 1200.0;

	Point pos = smoothtube.C(mark);
	activeCamera.pos.set(pos.x, pos.y, pos.z);

	Point eye = smoothtube.derivative(mark);
	activeCamera.eye.set(eye.x, eye.y, eye.z);

	Point lpos = smoothtube.C(mark + 0.05);
	light_pos.set(lpos.x, lpos.y, lpos.z);
#endif

	gluLookAt(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z,
		activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z,
		activeCamera.ori.x, activeCamera.ori.y, activeCamera.ori.z);
	glGetFloatv(GL_MODELVIEW_MATRIX, modelview_matrix_tmp);


	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			//modelview_matrix[4 * j + i] = modelview_matrix_tmp[4 * i + j];
			modelview_matrix[4 * i + j] = modelview_matrix_tmp[4 * i + j];
		}
	}


	init_uniforms();

	light_pos = old_light_pos;

	exit_on_gl_error("ERROR: 12 Could not set the shader uniforms");
	exit_on_gl_error("ERROR: Could not bind the VAO for drawing purposes");


	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);                     //
	glViewport(0, 0, cur_width, cur_height);                                     //
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);              //
	glBindVertexArray(buff_ids[0]);

	if (is_wireframe == 0){
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };                  // 
	glDrawBuffers(2, attachments);                                   //
	glDrawElements(GL_TRIANGLES, num_triangle * 3, GL_UNSIGNED_INT, (GLvoid*)0);
	GLfloat pixel[16];
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fb);             //
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	glReadPixels(cur_width / 2 - 1, cur_height / 2 - 1, 2, 2, GL_RGBA, GL_FLOAT, pixel);

	int collision = 0;
	for (int i = 0; i < 16; i++){
		if (i % 4 == 3)
			continue;
		if (pixel[i] > 0.5){
			collision = 1;
			break;
		}
	}

	if (collision == 0){
		glReadBuffer(GL_COLOR_ATTACHMENT1);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);             //
		glDrawBuffer(GL_FRONT);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glBlitFramebuffer(0, 0, cur_width, cur_height, 0, 0, cur_width, cur_height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
		glBindVertexArray(0);
		glUseProgram(0);
		//draw_eye_and_light();
		if (show_normals)
		{
			draw_normals();
		}
		if (show_tube == 1){
			draw_pipe();
		}
	}
	else{
		//printf("collision detected \n");
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);             //
		glClearColor(1.0, 0.0, 0.0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	exit_on_gl_error("ERROR: Could not draw the cube");


	isRender = 0;
	glutSwapBuffers();
	// glDisable(GL_STENCIL_TEST);	
}

void resize(int width, int height)
{
	camM = 1.0;
	glUseProgram(shad_ids[0]);
	cur_width = width;
	cur_height = height;
	glViewport(0, 0, cur_width, cur_height);
	glScissor(0, 0, cur_width, cur_height);
	free(pixel);
	pixel = new GLfloat[cur_width * cur_height * 4];

	//glClear(GL_STENCIL_BUFFER_BIT);
	//glStencilFunc(GL_ALWAYS, 0x1, 0x1);
	//glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
	//drawMask();

	glDeleteRenderbuffers(cnt, rb);
	glGenRenderbuffers(cnt, rb);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[col1]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, cur_width, cur_height);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[col2]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, cur_width, cur_height);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[dep]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, cur_width, cur_height);

	glDeleteFramebuffers(1, &fb);
	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);

	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_RENDERBUFFER, rb[col1]);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_RENDERBUFFER, rb[col2]);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER, rb[dep]);

	//proj_mat = proj(60, (float)cur_width/cur_height, 0.01f, 2000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0); 
	Camera3d activeCamera = freeCam;
	//gluPerspective(activeCamera.ang, activeCamera.ar, activeCamera.cnear, activeCamera.cfar);
	gluPerspective(30, 1, 0.1, 1000);
	//gluPerspective(60, (float)cur_width/cur_height, 0.01f, 2000.0f);
	exit_on_gl_error("ERROR: 1.Could not use the shader program");
	GLfloat matrix[16];
	glGetFloatv(GL_PROJECTION_MATRIX, matrix);
	GLfloat pmatrix[16];

	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			//pmatrix[4 * j + i] = matrix[4 * i + j];
			pmatrix[4 * i + j] = matrix[4 * i + j];
		}
	}

	glUniformMatrix4fv(proj_mat_uloc, 1, GL_FALSE, pmatrix);
	glUseProgram(0);
}

void myinit(int argc, char *argv[])
{
	GLenum glew_init_res;
	init_window(argc, argv);
	define_callbacks();
	glewExperimental = GL_TRUE;
	glew_init_res = glewInit();

	if (GLEW_OK != glew_init_res)
	{
		fprintf(stderr, "\nERROR: %s", glewGetErrorString(glew_init_res));
		exit(EXIT_FAILURE);
	}
	fprintf(stdout, "\nINFO: OpenGL Version: %s", glGetString(GL_VERSION));
	int multisampling_query = 1;
	glGetIntegerv(GL_SAMPLES, &multisampling_query);
	fprintf(stdout, "\nmultisampling_query: %d", multisampling_query);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glEnable(GL_SCISSOR_TEST);
	glScissor(0, 0, cur_width, cur_height);

	//glClearStencil(0x0);
	//glEnable(GL_STENCIL_TEST);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	exit_on_gl_error("ERROR: could not set OpenGL depth testing options");
	glEnable(GL_STENCIL_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CCW);
	exit_on_gl_error("ERROR: could not set OpenGL culling options");
	//glShadeModel( GL_SMOOTH);
	//glEnable( GL_NORMALIZE);	

	is_point_light = is_normal_mapping = is_gloss_mapping = is_mucous = show_patch = is_invert_narmal = is_internal = is_diffuse = is_specular = is_partial_blending = 1;
	which_texture = which_ref_map = which_gloss_map = 1;
	is_partial_blending = 1;
	is_wireframe = which_mucous = 0;

	process_model();
	handle_shader_program();
	load_textures();

	freeCam.pos.set(300.0f, 0.0f, 0.0f);
	//freeCam.pos.set( 0., 0., -50.);
	freeCam.cnear = 0.01f;
	freeCam.cfar = 2000.0f;
	freeCam.ang = 45.0 / camM;
	freeCam.ar = 1;
	freeCam.eye.set(0.0f, 0.0f, 0.0f);

	glGenRenderbuffers(cnt, rb);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[col1]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, 600, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[col2]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, 600, 600);
	glBindRenderbuffer(GL_RENDERBUFFER, rb[dep]);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 600, 600);

	glGenFramebuffers(1, &fb);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fb);

	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_RENDERBUFFER, rb[col1]);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_RENDERBUFFER, rb[col2]);
	glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_RENDERBUFFER, rb[dep]);

	glEnable(GL_DEPTH_TEST);
	glBindVertexArray(0);
	glUseProgram(0);
}

void process_model()
{
	fprintf(stderr, "\nbefore calling loading off\n");

	FILE *file_r, *file_temp_r;
	file_r = fopen("data_items/files/model_with_attributes.esff", "r");
	file_temp_r = fopen("data_items/files/walk_through.gclff", "rb");

	if (0) // always false
	{
		char temp[5];
		int tmp;
		fprintf(stderr, "\n **** Reading entire model with all attributes from file ***");
		fscanf(file_r, "%s", temp);
		fscanf(file_r, "%d %d %d", &mod.num_vert, &mod.num_triangle, &tmp);
		fprintf(stderr, "\n num of vertices %d and poly %d", mod.num_vert, mod.num_triangle);

		mod.vers = new ver[mod.num_vert];
		mod.triangles = new model_triangle[mod.num_triangle];

		for (int i = 0; i < mod.num_vert; i++){
			fscanf(file_r, "%f %f %f", &mod.vers[i].pos.x, &mod.vers[i].pos.y, &mod.vers[i].pos.z);
			fscanf(file_r, "%f %f %f", &mod.vers[i].nor.x, &mod.vers[i].nor.y, &mod.vers[i].nor.z);
			fscanf(file_r, "%f %f", &mod.vers[i].uv_texture.x, &mod.vers[i].uv_texture.y);
			fscanf(file_r, "%f %f %f", &mod.vers[i].tangent.x, &mod.vers[i].tangent.y, &mod.vers[i].tangent.z);
			fscanf(file_r, "%f %f %f", &mod.vers[i].patch_col_texture.x, &mod.vers[i].patch_col_texture.y, &mod.vers[i].patch_col_texture.z);
			fscanf(file_r, "%f %f", &mod.vers[i].bound_uv_texture1.x, &mod.vers[i].bound_uv_texture1.y);
			fscanf(file_r, "%f", &mod.vers[i].is_partial_patch);
			fscanf(file_r, "%f", &mod.vers[i].partial_blending);
			fscanf(file_r, "%f", &mod.vers[i].is_mucous_present);
		}

		for (int i = 0; i < mod.num_triangle; i++){
			fscanf(file_r, "%d %d %d %d \n", &tmp, &mod.triangles[i].ver0, &mod.triangles[i].ver1, &mod.triangles[i].ver2);
		}

		fclose(file_r);
		fclose(file_temp_r);

		lWalkthrough = makeCenterLine(mod, -52.1f, 20.0f, 1, smoothtube);
	}
	else{
		mod = loadOFF("data_items/files/model_exp4.off");
		lWalkthrough = makeCenterLine(mod, -52.1f, 20.0f, 1, smoothtube);
		graph grp = find_graph(&mod);
		//debug_graph(grp);
		LapTexture *lt = new LapTexture(grp);
		grp = (*lt).assign_laptexture();
		/*
		FILE *file_w = fopen("data_items/files/model_with_attributes.esff", "w");

		if (file_w)
		{
		fprintf(stderr, "\n **** Model is written into file with all vertex attributes ***");
		fprintf(file_w, "%s \n", "ESFF");
		fprintf(file_w, "%d %d %d \n", grp.mod->num_vert, grp.mod->num_triangle, 0);

		for (unsigned i = 0; i < grp.mod->num_vert; i++){
		fprintf(file_w, "%f %f %f \n", grp.mod->vers[i].pos.x, grp.mod->vers[i].pos.y, grp.mod->vers[i].pos.z);
		fprintf(file_w, "%f %f %f \n", grp.mod->vers[i].nor.x, grp.mod->vers[i].nor.y, grp.mod->vers[i].nor.z);
		fprintf(file_w, "%f %f \n", grp.mod->vers[i].uv_texture.x, grp.mod->vers[i].uv_texture.y);
		fprintf(file_w, "%f %f %f \n", grp.mod->vers[i].tangent.x, grp.mod->vers[i].tangent.y, grp.mod->vers[i].tangent.z);
		fprintf(file_w, "%f %f %f \n", grp.mod->vers[i].patch_col_texture.x, grp.mod->vers[i].patch_col_texture.y, grp.mod->vers[i].patch_col_texture.z);
		fprintf(file_w, "%f %f \n", grp.mod->vers[i].bound_uv_texture1.x, grp.mod->vers[i].bound_uv_texture1.y);
		fprintf(file_w, "%f \n", grp.mod->vers[i].is_partial_patch);
		fprintf(file_w, "%f \n", grp.mod->vers[i].partial_blending);
		fprintf(file_w, "%f \n", grp.mod->vers[i].is_mucous_present);
		}

		for (unsigned i = 0; i < grp.mod->num_triangle; i++){
		fprintf(file_w, "%d %d %d %d \n", 3, grp.mod->triangles[i].ver0, grp.mod->triangles[i].ver1, grp.mod->triangles[i].ver2);
		}
		fclose(file_w);
		}
		*/
	}

	//for(int i = 0; i < lWalkthrough.num_pos; i++)
	//fprintf(stderr,"\n (%f, %f, %f);",lWalkthrough.pts[3*i + 0], lWalkthrough.pts[3*i + 1], lWalkthrough.pts[3*i + 2]);
	//mod = gen_sinusoidal();

	ver *verts = mod.vers;
	model_triangle *indx = mod.triangles;
	num_triangle = mod.num_triangle;
	unsigned num_vert = mod.num_vert;

	fprintf(stderr, "\nnum_tr = %u num_verts = %u\n", num_triangle, num_vert);
	//fprintf(stderr,"\size of ver = %u size of verts[0] = %u size of verts[0].pos = %u\n", sizeof(verts), sizeof(verts[0]), sizeof(verts[0].pos));
	fprintf(stderr, "\nafter calling loading off\n");
}

void render()
{
	frame_cnt++;
	draw_model();

}

void idle()
{
	//if (isRender == 1)
	glutPostRedisplay();
}

void timer_func(int val)
{
	if (val != 0)
	{
		char *tmp = (char *)malloc(512 + strlen(WINDOW_TITLE_PREFIX));
		sprintf(tmp, "%s", "Virtual Endoscopy");
		glutSetWindowTitle(tmp);
		free(tmp);
	}
	frame_cnt = 0;
	glutTimerFunc(250, timer_func, 1);
}

void define_callbacks()
{
	glutReshapeFunc(resize);
	glutDisplayFunc(render);
	glutIdleFunc(idle);
	glutTimerFunc(0, timer_func, 0);
	glutCloseFunc(destroy_model);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mousefunc);
	glutMotionFunc(mousemotionfunc);
	glutPassiveMotionFunc(mousepass);
	glutMouseWheelFunc(mouseWheel);
}

void create_and_link_shader(){
	shad_ids[0] = glCreateProgram();
	exit_on_gl_error("ERROR: Could not create the shader program");
	{
		shad_ids[1] = load_shad("shaders/fragment.glsl", GL_FRAGMENT_SHADER);
		shad_ids[2] = load_shad("shaders/vertex.glsl", GL_VERTEX_SHADER);
		glAttachShader(shad_ids[0], shad_ids[1]);
		glAttachShader(shad_ids[0], shad_ids[2]);
	}
	glLinkProgram(shad_ids[0]);
	exit_on_gl_error("ERROR: Could not link the shader program");
}

void get_uniforms(){
	model_mat_uloc = glGetUniformLocation(shad_ids[0], "model_mat");
	normal_mat_uloc = glGetUniformLocation(shad_ids[0], "normal_mat");
	proj_mat_uloc = glGetUniformLocation(shad_ids[0], "proj_mat");
	dir_light_dir_uloc = glGetUniformLocation(shad_ids[0], "dir_light_dir");
	light_pos_uloc = glGetUniformLocation(shad_ids[0], "light_pos");
	is_point_light_uloc = glGetUniformLocation(shad_ids[0], "is_point_light");
	cons_atten_uloc = glGetUniformLocation(shad_ids[0], "cons_atten");
	lin_atten_uloc = glGetUniformLocation(shad_ids[0], "lin_atten");
	qd_atten_uloc = glGetUniformLocation(shad_ids[0], "qd_atten");
	texture_scalling_uloc = glGetUniformLocation(shad_ids[0], "texture_scalling");
	partial_blending_uloc = glGetUniformLocation(shad_ids[0], "partial_blending");
	light_col_uloc = glGetUniformLocation(shad_ids[0], "light_col");
	amb_light_col_uloc = glGetUniformLocation(shad_ids[0], "amb_light_col");
	shiny_uloc = glGetUniformLocation(shad_ids[0], "shiny");
	is_normal_mapping_uloc = glGetUniformLocation(shad_ids[0], "is_normal_mapping");
	is_gloss_mapping_uloc = glGetUniformLocation(shad_ids[0], "is_gloss_mapping");
	is_mucous_uloc = glGetUniformLocation(shad_ids[0], "is_mucous");
	show_patch_uloc = glGetUniformLocation(shad_ids[0], "show_patch");
	is_invert_narmal_uloc = glGetUniformLocation(shad_ids[0], "is_invert_narmal");
	which_texture_uloc = glGetUniformLocation(shad_ids[0], "which_texture");
	which_ref_map_uloc = glGetUniformLocation(shad_ids[0], "which_ref_map");
	which_gloss_map_uloc = glGetUniformLocation(shad_ids[0], "which_gloss_map");
	which_mucous_uloc = glGetUniformLocation(shad_ids[0], "which_mucous");
	is_diffuse_uloc = glGetUniformLocation(shad_ids[0], "is_diffuse");
	is_specular_uloc = glGetUniformLocation(shad_ids[0], "is_specular");

	texture_uloc = glGetUniformLocation(shad_ids[0], "texture");
	ref_map_uloc = glGetUniformLocation(shad_ids[0], "ref_map");
	gloss_map_uloc = glGetUniformLocation(shad_ids[0], "gloss_map");
	mucous_uloc = glGetUniformLocation(shad_ids[0], "mucous");
	texture_tmp_uloc = glGetUniformLocation(shad_ids[0], "texture_tmp");
	ref_map_tmp_uloc = glGetUniformLocation(shad_ids[0], "ref_map_tmp");
	gloss_map_tmp_uloc = glGetUniformLocation(shad_ids[0], "gloss_map_tmp");
	mucous_tmp_uloc = glGetUniformLocation(shad_ids[0], "mucous_tmp");

	is_partial_blending_uloc = glGetUniformLocation(shad_ids[0], "is_partial_blending");

	exit_on_gl_error("ERROR: Could not get the shader uniform locations");
}

void init_uniforms()
{
	exit_on_gl_error("ERROR: 2.Could not use the shader program");
	glUniformMatrix4fv(model_mat_uloc, 1, GL_FALSE, modelview_matrix);
	glUniformMatrix4fv(normal_mat_uloc, 1, GL_FALSE, modelview_matrix);
	GLfloat light_pos_float[3] = { light_pos.x, light_pos.y, light_pos.z };
	glUniform3fv(light_pos_uloc, 1, light_pos_float);
	glUniform3fv(dir_light_dir_uloc, 1, dir_light_dir);
	glUniform3fv(amb_light_col_uloc, 1, amb_light_col);
	glUniform1i(is_point_light_uloc, is_point_light);      //shifted in draw_cube because now it can be controlled by keyboard
	glUniform3fv(light_col_uloc, 1, light_col);
	glUniform1f(cons_atten_uloc, cons_atten);
	glUniform1f(lin_atten_uloc, lin_atten);
	glUniform1f(qd_atten_uloc, qd_atten);
	glUniform1f(texture_scalling_uloc, texture_scalling);
	glUniform1f(partial_blending_uloc, partial_blending);
	glUniform1i(shiny_uloc, shiny);
	glUniform1i(is_normal_mapping_uloc, is_normal_mapping); //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(is_gloss_mapping_uloc, is_gloss_mapping);   //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(is_mucous_uloc, is_mucous); //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(show_patch_uloc, show_patch); //shifted in draw_cube because now it can be controlled by keyboard

	glUniform1i(is_invert_narmal_uloc, is_invert_narmal);      //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(is_specular_uloc, is_specular);
	glUniform1i(is_diffuse_uloc, is_diffuse);
	glUniform1i(which_texture_uloc, which_texture); //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(which_ref_map_uloc, which_ref_map);   //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(which_gloss_map_uloc, which_gloss_map);      //shifted in draw_cube because now it can be controlled by keyboard
	glUniform1i(which_mucous_uloc, which_mucous);
	glUniform1i(is_partial_blending_uloc, is_partial_blending);
}

void gen_ver_array(){           // generate two vertex arrays, for model and circular mask
	glGenVertexArrays(2, &buff_ids[0]);
	exit_on_gl_error("ERROR: Could not generate the VAO");
}

void populate_buffer_for_model(){

	glBindVertexArray(buff_ids[0]);
	exit_on_gl_error("ERROR: Could not bind the VAO");
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);
	glEnableVertexAttribArray(8);

	exit_on_gl_error("ERROR: Could not generate the index for vertex attributes");
	glGenBuffers(2, &buff_ids[2]);
	exit_on_gl_error("ERROR: Could not generate the buffer objects");

	const ver vert_sample[1] =
	{
		{ { -.5f, -.5f, .5f }, { 0, 0, 1 }, { 1, 1 }, { 1, 0, 6 }, { 1, 1, 1 }, { 1 } },
	};

	glBindBuffer(GL_ARRAY_BUFFER, buff_ids[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert_sample[0]) * mod.num_vert, mod.vers, GL_STATIC_DRAW);
	exit_on_gl_error("ERROR: Could not bind the VBO to the VAO");

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture)));
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent)));
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture)));
	glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1)));
	glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1) + sizeof(vert_sample[0].is_partial_patch)));
	glVertexAttribPointer(8, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1) + sizeof(vert_sample[0].is_partial_patch) + sizeof(vert_sample[0].partial_blending)));

	exit_on_gl_error("ERROR: Could not set VAO attributes");

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buff_ids[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) *  mod.num_triangle * 3, mod.triangles, GL_STATIC_DRAW);
	exit_on_gl_error("ERROR: Could not bind the IBO to the VAO");
	glBindVertexArray(0);
}

void populate_buffer_for_circular_mask(){

	ver *verts1;
	GLuint *indx1;
	int num_vert_circle = num_triangle_circle * 3;
	verts1 = new ver[num_vert_circle];
	indx1 = new GLuint[num_triangle_circle * 3];
	double wd = 0.8;
	double prevCos = 1;
	double prevSin = 0;

	for (unsigned k = 0; k<num_triangle_circle; k++)
	{
		float cosin = cos((k + 1)*(2 * 3.1415) / num_triangle_circle);
		float sine = sin((k + 1)*(2 * 3.1415) / num_triangle_circle);

		verts1[3 * k + 0].pos.set(wd*cosin, wd*sine, -0.5);
		verts1[3 * k + 1].pos.set(0, 0, -0.5);
		verts1[3 * k + 2].pos.set(wd*prevCos, wd*prevSin, -0.5);

		indx1[3 * k + 0] = 3 * k + 0;
		indx1[3 * k + 1] = 3 * k + 1;
		indx1[3 * k + 2] = 3 * k + 2;

		prevCos = cosin;
		prevSin = sine;
	}

	const ver vert_sample[1] =
	{
		{ { -.5f, -.5f, .5f }, { 0, 0, 1 }, { 1, 1 }, { 1, 0, 6 }, { 1, 1, 1 }, { 1 } },
	};

	glBindVertexArray(buff_ids[1]);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);
	glEnableVertexAttribArray(6);
	glEnableVertexAttribArray(7);
	glEnableVertexAttribArray(8);


	glGenBuffers(2, &buff_ids[4]);
	glBindBuffer(GL_ARRAY_BUFFER, buff_ids[4]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vert_sample[0]) * num_vert_circle, verts1, GL_STATIC_DRAW);
	exit_on_gl_error("ERROR: Could not bind the VBO to the VAO");

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos)));
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor)));
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture)));
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent)));
	glVertexAttribPointer(5, 2, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture)));
	glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1)));
	glVertexAttribPointer(7, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1) + sizeof(vert_sample[0].is_partial_patch)));
	glVertexAttribPointer(8, 1, GL_FLOAT, GL_FALSE, sizeof(vert_sample[0]), (GLvoid*)(sizeof(vert_sample[0].pos) + sizeof(vert_sample[0].nor) + sizeof(vert_sample[0].uv_texture) + sizeof(vert_sample[0].tangent) + sizeof(vert_sample[0].patch_col_texture) + sizeof(vert_sample[0].bound_uv_texture1) + sizeof(vert_sample[0].is_partial_patch) + sizeof(vert_sample[0].partial_blending)));
	exit_on_gl_error("ERROR: Could not set VAO attributes");

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buff_ids[5]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * num_triangle_circle * 3, indx1, GL_STATIC_DRAW);
	exit_on_gl_error("ERROR: Could not bind the IBO to the VAO");
	glBindVertexArray(0);
}

void handle_shader_program(){
	create_and_link_shader();
	get_uniforms();
	gen_ver_array();
	populate_buffer_for_model();
	populate_buffer_for_circular_mask();
}

void process_texture(const char * imagepath, int texture_unit, GLuint *texture_id, GLuint uloc){
	image texture = loadBMP_custom(imagepath);
	glGenTextures(1, texture_id);
	glBindTexture(GL_TEXTURE_2D, *texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture.width, texture.height, 0, GL_BGR, GL_UNSIGNED_BYTE, texture.pixels);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);	// Set the preferences
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);
	glUniform1i(uloc, texture_unit);
}

void load_textures()
{
	glUseProgram(shad_ids[0]);
	exit_on_gl_error("ERROR: 4.Could not use the shader program");

	int tex_unit = 0;

	glActiveTexture(GL_TEXTURE0);
	process_texture(texture_path, tex_unit, &tex_id, texture_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE1);
	process_texture(reflectance_map_path, tex_unit, &ref_map_id, ref_map_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE2);
	process_texture(gloss_map_path, tex_unit, &gloss_map_id, gloss_map_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE3);
	process_texture(texture_tmp_path, tex_unit, &tex_id, texture_tmp_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE4);
	process_texture(reflectance_map_tmp_path, tex_unit, &ref_map_id, ref_map_tmp_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE5);
	process_texture(gloss_map_tmp_path, tex_unit, &gloss_map_id, gloss_map_tmp_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE6);
	process_texture(mucous_path, tex_unit, &mucous_id, mucous_uloc);
	tex_unit++;

	glActiveTexture(GL_TEXTURE7);
	process_texture(mucous_tmp_path, tex_unit, &mucous_id, mucous_tmp_uloc);
	tex_unit++;

	glUseProgram(0);
}

void destroy_model(void)
{
	glDetachShader(shad_ids[0], shad_ids[1]);
	glDetachShader(shad_ids[0], shad_ids[2]);
	glDeleteShader(shad_ids[1]);
	glDeleteShader(shad_ids[2]);
	glDeleteProgram(shad_ids[0]);
	exit_on_gl_error("ERROR: Could not destroy the shaders");
	glDeleteBuffers(2, &buff_ids[1]);
	glDeleteVertexArrays(1, &buff_ids[0]);
	exit_on_gl_error("ERROR: Could not destroy the buffer objects");
}

void draw_eye_and_light(){
	float dist = 0.5f;

	glBegin(GL_LINES);

	glColor3f(0.0, 1.0, 0.0);    //  eye position

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x + dist, activeCamera.pos.y, activeCamera.pos.z);

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x - dist, activeCamera.pos.y, activeCamera.pos.z);

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x, activeCamera.pos.y + dist, activeCamera.pos.z);

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x, activeCamera.pos.y - dist, activeCamera.pos.z);

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z + dist);

	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z);
	glVertex3f(activeCamera.pos.x, activeCamera.pos.y, activeCamera.pos.z - dist);


	/*
	glColor3f(1.0, 1.0, 1.0);    // light source position in blue

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0] + dist, light_pos[1], light_pos[2]);

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0] - dist, light_pos[1], light_pos[2]);

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0], light_pos[1] + dist, light_pos[2]);

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0], light_pos[1] - dist, light_pos[2]);

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0], light_pos[1], light_pos[2] + dist);

	glVertex3f(light_pos[0], light_pos[1], light_pos[2]);
	glVertex3f(light_pos[0], light_pos[1], light_pos[2] - dist);


	glColor3f(0.0, 1.0, 1.0);    // center position in green

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x + dist, activeCamera.eye.y, activeCamera.eye.z);

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x - dist, activeCamera.eye.y, activeCamera.eye.z);

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x, activeCamera.eye.y + dist, activeCamera.eye.z);

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x, activeCamera.eye.y - dist, activeCamera.eye.z);

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z + dist);

	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z);
	glVertex3f(activeCamera.eye.x, activeCamera.eye.y, activeCamera.eye.z - dist);
	*/
	glEnd();
	//glFlush();
}

void draw_normals(){
	int dir = 1;
	if (is_invert_narmal){
		dir = -1;
	}

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_LINES);
	for (int i = 0; i < mod.num_vert; i++){
		glVertex3f(mod.vers[i].pos.x + .1 * dir * mod.vers[i].nor.x, mod.vers[i].pos.y + .1 * dir * mod.vers[i].nor.y, mod.vers[i].pos.z + .1 * dir * mod.vers[i].nor.z);
		glVertex3f(mod.vers[i].pos.x, mod.vers[i].pos.y, mod.vers[i].pos.z);

		//glVertex3f(mod.vers[i].pos[0] + .001, mod.vers[i].pos[1] + .001, mod.vers[i].pos[2] + .001);
	}

	/*
	glColor3f(0.0, 1.0, 0.0);

	for (int i = 0; i < mod.num_vert; i++){
	glVertex3f(mod.vers[i].pos[0], mod.vers[i].pos[1], mod.vers[i].pos[2]);
	glVertex3f(mod.vers[i].pos[0] - 1 * mod.vers[i].tangent[0], mod.vers[i].pos[1] - 1 * mod.vers[i].tangent[1], mod.vers[i].pos[2] - 1 * mod.vers[i].tangent[2]);

	//glVertex3f(mod.vers[i].pos[0] + .001, mod.vers[i].pos[1] + .001, mod.vers[i].pos[2] + .001);
	}
	*/
	glEnd();
}

void cleanupQuadric(void)          // Properly Kill The Window
{
	gluDeleteQuadric(IDquadric);
	printf("cleanupQuadric completed\n");
}

void draw_pipe(){

	int num_vert_circle = num_triangle_circle * 3;
	ver *verts1 = new ver[num_vert_circle];
	GLuint *indx1 = new GLuint[num_triangle_circle * 3];
	double wd = 0.8;
	double prevCos = 1;
	double prevSin = 0;

	glBindVertexArray(0);
	glUseProgram(0);

	GLfloat blackDiffuseMaterial[] = { 0.8, 0.8, .8 }; //set the material to red
	GLfloat whiteSpecularMaterial[] = { 1.0, 1.0, 1.0 }; //set the material to white

	GLfloat whiteSpecularLight[] = { 1.0, 1.0, 1.0 }; //set the light specular to white
	GLfloat blackAmbientLight[] = { 0.0, 0.0, 0.0 }; //set the light ambient to black
	GLfloat whiteDiffuseLight[] = { 1.0, 1.0, 1.0 }; //set the diffuse light to white
	GLfloat ambCol[] = { .2, .2, .2 };

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, (GLfloat*)&whiteSpecularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, (GLfloat*)&blackAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, (GLfloat*)&whiteDiffuseLight);

	float light_pos_l[4] = { light_pos.x, light_pos.y, light_pos.z, 1 };
	glLightfv(GL_LIGHT0, GL_POSITION, (GLfloat*)&light_pos_l);


	//glutWarpPointer( WINX/2, WINY/2);
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambCol);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);

	GLfloat mShininess[] = { 25 };

	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, whiteSpecularMaterial);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, blackDiffuseMaterial);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mShininess);


	int numMajor = 6, numMinor = 16;
	float height = 10, radius = 0.1;

	double majorStep = height / numMajor;
	double minorStep = 2.0 * PI / numMinor;
	float step = 1 / 100.0;
	Point pt1, pt2;
	int i = 0;
	for (; i < 99; i++) {
		//GLfloat z0 = 0.5 * height - i * majorStep;
		//GLfloat z1 = z0 - majorStep;
		pt1 = smoothtube.C(i*step);
		pt2 = smoothtube.C((i + 1)*step);

		if (pt2.z > freeCam.pos.z - 10)
			break;

		glBegin(GL_TRIANGLE_STRIP);
		for (int j = 0; j <= numMinor; ++j) {
			double a = j * minorStep;
			GLfloat x = radius * cos(a);
			GLfloat y = radius * sin(a);

			glNormal3f(x, y, 0.0);
			//glTexCoord2f(j / (GLfloat)numMinor, i / (GLfloat)numMajor);
			glVertex3f(x + pt1.x, y + pt1.y, pt1.z);

			glNormal3f(x, y, 0.0);
			//glTexCoord2f(j / (GLfloat)numMinor, z_start + (i + 1) / (GLfloat)numMajor);
			glVertex3f(x + pt2.x, y + pt2.y, pt2.z);
		}
		glEnd();
	}


	BSpline tubeHead;

	int len = 6;

	Point *pts = new Point[100];

	Point pt;



	int j = 0;
	for (;; j++)
	{
		pt = smoothtube.C(i*step);
		if (pt.z > freeCam.pos.z + .3)
			break;


		pts[j].x = pt.x;
		pts[j].y = pt.y;
		pts[j].z = pt.z;
		i++;
	}


	pts[j].x = freeCam.pos.x;
	pts[j].y = freeCam.pos.y;
	pts[j].z = freeCam.pos.z;

	if (j < 5)
		return;

	if (j > 25)
		tubeHead.initCurve(20, 3);
	else if (j > 20)
		tubeHead.initCurve(15, 3);
	else if (j > 15)
		tubeHead.initCurve(10, 3);
	else
		tubeHead.initCurve(5, 3);



	tubeHead.lsApprox(j, pts);
	delete[] pts;

	for (int i = 0; i < 95; i++) {
		//GLfloat z0 = 0.5 * height - i * majorStep;
		//GLfloat z1 = z0 - majorStep;
		pt1 = tubeHead.C(i*step);
		pt2 = tubeHead.C((i + 1)*step);

		glBegin(GL_TRIANGLE_STRIP);
		for (int j = 0; j <= numMinor; ++j) {
			double a = j * minorStep;
			GLfloat x = radius * cos(a);
			GLfloat y = radius * sin(a);

			glNormal3f(x, y, 0.0);
			//glTexCoord2f(j / (GLfloat)numMinor, i / (GLfloat)numMajor);
			glVertex3f(x + pt1.x, y + pt1.y, pt1.z);

			glNormal3f(x, y, 0.0);
			//glTexCoord2f(j / (GLfloat)numMinor, z_start + (i + 1) / (GLfloat)numMajor);
			glVertex3f(x + pt2.x, y + pt2.y, pt2.z);
		}
		glEnd();
	}


}

void init_window(int argc, char * argv[])
{
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
	glutInitWindowSize(cur_width, cur_height);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_STENCIL);
	win_handle = glutCreateWindow(WINDOW_TITLE_PREFIX);

	if (win_handle < 1)
	{
		fprintf(stderr, "\nERROR: Could not create a new rendering window.");
		exit(EXIT_FAILURE);
	}

}

int main(int argc, char * argv[])
{
	myinit(argc, argv);
#ifdef INTEGRATION
	//Open already created shared memory object.
	windows_shared_memory shm(open_only, "shm", read_only);

	//Map the whole shared memory in this process
	mapped_region region(shm, read_only);

	//read character from region
	myint = static_cast<int*>(region.get_address());
#endif
	glutMainLoop();
	exit(EXIT_SUCCESS);
}