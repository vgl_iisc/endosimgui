/*
File : mod_utils.cpp
Author : Tarun Bansal
Description : functions related to model
*/

#include "mod_utils.h"

model loadOFF(const char *offfile){

	fprintf(stderr, "\n **** model loading starts***");
	int num_triangle, tmp;
	int num_vertices, num_normals;
	ver *verts;
	//GLuint *indx;
	model_triangle *triangles;
	char temp[5];
	model mod;

	FILE *file = fopen(offfile, "rb");
	if (file)
	{
		fprintf(stderr, "\n yes reading");
		fscanf(file, "%s", temp);
		fscanf(file, "%d %d %d", &num_vertices, &num_triangle, &tmp);
		fprintf(stderr, "\n num of vertices %d and poly %d", num_vertices, num_triangle);
		//num_indices = 3 * num_face;
		num_normals = num_vertices;
		//*num_triangle = num_face;
		verts = new ver[num_vertices];
		//indx = new GLuint[num_indices];
		triangles = new model_triangle[num_triangle];

		float hx, lx = 1000, hy, ly = 1000, hz, lz = 1000;
		hx = hy = hz = -10000.0f;

		for (int i = 0; i < num_vertices; i++)
		{
			fscanf(file, "%f %f %f", &verts[i].pos.x, &verts[i].pos.y, &verts[i].pos.z);
			//verts[i].pos[2] = -1 * verts[i].pos[2];

			verts[i].patch_col_texture.set(0.0, 0.0, 0.0); // [0] = 0.0;  verts[i].patch_col_texture[1] = 0.0; verts[i].patch_col_texture[2] = 0.0;
			
			if (hx <  verts[i].pos.x)
				hx = verts[i].pos.x;
			if (lx > verts[i].pos.x)
				lx = verts[i].pos.x;
			if (hy < verts[i].pos.y)
				hy = verts[i].pos.y;
			if (ly > verts[i].pos.y)
				ly = verts[i].pos.y;
			if (hz < verts[i].pos.z)
				hz = verts[i].pos.z;
			if (lz > verts[i].pos.z)
				lz = verts[i].pos.z;
		}
		printf("\n hx = %f  lx = %f  hy =%f  ly = %f hz = %f lz = %f ", hx, lx, hy, ly, hz, lz);

		float xdiff = hx - lx;
		float ydiff = hy - ly;
		float zdiff = hz - lz;
		float xtrans = (hx + lx) / 2;
		float ytrans = (hy + ly) / 2;
		float ztrans = (hz + lz) / 2;
		float scale = .1f;//10/(max(xdiff,ydiff,zdiff)); 
		lx = ly = lz = 10000.0f;
		hx = hy = hz = -10000.0f;

		for (int i = 0; i < num_vertices; i++)
		{
			verts[i].pos = (verts[i].pos - Vector3f(xtrans, ytrans, ztrans)) * Vector3f(scale, scale, scale);

		//	verts[i].pos[0] = (verts[i].pos[0] - xtrans)*scale;
		//	verts[i].pos[1] = (verts[i].pos[1] - ytrans)*scale;
		//	verts[i].pos[2] = (verts[i].pos[2] - ztrans)*scale;

			verts[i].uv_texture.set(verts[i].pos.x * 1, verts[i].pos.y * 1);

		//	verts[i].uv_texture[0] =  verts[i].pos[1] * 1;
		//	verts[i].uv_texture[1] =   verts[i].pos[2] * 1;

			verts[i].nor.set(0.0, 0.0, 0.0);
			verts[i].tangent.set(0.0, 0.0, 0.0);

		//	verts[i].nor[0] = verts[i].nor[1] = verts[i].nor[2] = 0;
		//	verts[i].tangent[0] = verts[i].tangent[1] = verts[i].tangent[2] = 0;
			verts[i].is_mucous_present = 1.0;

			if (hx < verts[i].pos.x)
				hx = verts[i].pos.x;
			if (lx > verts[i].pos.x)
				lx = verts[i].pos.x;
			if (hy < verts[i].pos.y)
				hy = verts[i].pos.y;
			if (ly > verts[i].pos.y)
				ly = verts[i].pos.y;
			if (hz < verts[i].pos.z)
				hz = verts[i].pos.z;
			if (lz > verts[i].pos.z)
				lz = verts[i].pos.z;
		}
		printf("\n NEW...  hx = %f  lx = %f  hy =%f  ly = %f hz = %f lz = %f ", hx, lx, hy, ly, hz, lz);

		for (int i = 0; i < num_triangle; i++)
		{
			fscanf(file, "%d %u %u %u", &tmp, &triangles[i].ver0, &triangles[i].ver1, &triangles[i].ver2);

			int v0 = triangles[i].ver0, v1 = triangles[i].ver1, v2 = triangles[i].ver2;
			//GLfloat nor[3], f_edge[3], s_edge[3];

			Vector3f nor, f_edge, s_edge;
				
			f_edge = verts[v0].pos - verts[v1].pos;
			s_edge = verts[v0].pos - verts[v2].pos;

			nor =  f_edge ^ s_edge;
			nor = nor.normalize();

			//calCrossProd(nor, f_edge, s_edge);
			//doNormalize (nor,3);

			float ang_v0, ang_v1, ang_v2;

			// ang_v0 calculation
			
			f_edge = verts[v0].pos - verts[v1].pos;
			s_edge = verts[v0].pos - verts[v2].pos;
			ang_v0 = acos(f_edge & s_edge);

			// ang_v1 calculation
			
			f_edge = verts[v1].pos - verts[v2].pos;
			s_edge = verts[v1].pos - verts[v0].pos;
			ang_v1 = acos(f_edge & s_edge);

			// ang_2 calculation
			
			f_edge = verts[v2].pos - verts[v0].pos;
			s_edge = verts[v2].pos - verts[v1].pos;
			ang_v2 = acos(f_edge & s_edge);

			if (ang_v0 >= 3.14 || ang_v1 >= 3.14 || ang_v2 >= 3.14)
				printf("\nProblem\n");

			verts[v0].nor += Vector3f(ang_v0, ang_v0, ang_v0) * nor;
			verts[v1].nor += Vector3f(ang_v1, ang_v1, ang_v1) * nor;
			verts[v2].nor += Vector3f(ang_v2, ang_v2, ang_v2) * nor;
		}

		for (int i = 0; i < num_normals; i++)
		{
			float nor_len = verts[i].nor.magnitude(); // sqrt(verts[i].nor[0] * verts[i].nor[0] + verts[i].nor[1] * verts[i].nor[1] + verts[i].nor[2] * verts[i].nor[2]);
		
			if (nor_len > 0)
			{				
				verts[i].nor = verts[i].nor / Vector3f(nor_len, nor_len, nor_len);
			}
		}

	}
	else
	{
		printf("\n could not open file");
	}
	//printf("\n%f  %f  %f", verts[0].pos[0], verts[0].pos[1], verts[0].pos[2]);
	//printf("\n%f  %f  %f", verts[38].pos[0], verts[38].pos[1], verts[38].pos[2]);
	fclose(file);
	mod.triangles = triangles;
	mod.num_triangle = num_triangle;
	mod.num_vert = num_vertices;
	mod.vers = verts;
	fprintf(stderr, "\n **** model loading ends***\n");
	return mod;
}

void cal_tangent(model mod)
{
	fprintf(stderr, "\n **** tangent calculation starts***");
	unsigned int num_triangle = mod.num_triangle;
	unsigned int num_vert = mod.num_vert;
	ver *verts = mod.vers;
	model_triangle *triangles = mod.triangles;

	for (unsigned i = 0; i < num_triangle; i++)
	{
		// calculation of tangent
		int v0 = triangles[i].ver0, v1 = triangles[i].ver1, v2 = triangles[i].ver2;
		Vector3f ver0, ver1, ver2;
		Vector2f uv0, uv1, uv2;

		ver0 = verts[v0].pos;
		ver1 = verts[v1].pos;
		ver2 = verts[v2].pos;

		uv0 = verts[v0].uv_texture;
		uv1 = verts[v1].uv_texture;
		uv2 = verts[v2].uv_texture;
	
		Vector3f edge1, edge2;
		
		edge1 = ver1 - ver0;
		edge2 = ver2 - ver0;
	
		GLfloat deltaU1, deltaU2, deltaV1, deltaV2;
		deltaU1 = uv1.x - uv0.x;
		deltaV1 = uv1.y - uv0.y;
		deltaU2 = uv2.x - uv0.x;
		deltaV2 = uv2.y - uv0.y;

		//float f = (float) 1.0 / (deltaU1*deltaV2 - deltaU2*deltaV1);
		float f = (float)(deltaU1*deltaV2 - deltaU2*deltaV1);
		Vector3f tangent;

		tangent = (Vector3f(deltaV2, deltaV2, deltaV2) * edge1 - Vector3f(deltaV1, deltaV1, deltaV1) * edge2) / Vector3f(f, f, f);
		
		//doNormalize (tangent,3);
		
		verts[v0].tangent += tangent;
		verts[v1].tangent += tangent;
		verts[v2].tangent += tangent;
	
	}

	for (unsigned i = 0; i < num_vert; i++)
	{
		float tangent_len = verts[i].tangent.magnitude(); //  sqrt(verts[i].tangent[0] * verts[i].tangent[0] + verts[i].tangent[1] * verts[i].tangent[1] + verts[i].tangent[2] * verts[i].tangent[2]);
		if (tangent_len > .0000001)
		{
			verts[i].tangent = verts[i].tangent / Vector3f(tangent_len, tangent_len, tangent_len);
		}
		else{
			verts[i].tangent = Vector3f(1.0, 0, 0);
		}
	}

	fprintf(stderr, "\n **** tangent calculation ends***\n");
}

cam_path makeCenterLine(const  model &tube, float start, float end, float gap, BSpline &smoothtube){
	fprintf(stderr, "\n ****center line calculation starts***");

	cam_path lWalkthrough;
	lWalkthrough.num_pos = (unsigned)(end - start) / gap;

	FILE *file_r = fopen("data_items/files/walk_through.gclff", "rb");

	if (file_r)
	{
		fprintf(stderr, "\n ****center line is getting read from file***");
		fscanf(file_r, "%d", &lWalkthrough.num_pos);
		lWalkthrough.pts = new Vector3f[lWalkthrough.num_pos];
		for (unsigned i = 0; i < lWalkthrough.num_pos; i++){
			fscanf(file_r, "%f %f %f", &lWalkthrough.pts[i].x, &lWalkthrough.pts[i].y, &lWalkthrough.pts[i].z);
		}
		fclose(file_r);
		fprintf(stderr, "\n ****center line calculation ends here***");
		
	}
	else
	{

		Vector3f  p, p1, p2;
		float u;
		Vector3f last_point;
		last_point.set(0.0, 0.0, 0.0);
		float thrshd_sq = 20;
		fprintf(stderr, "\n   num_pos = %u   num_triangle = %u", lWalkthrough.num_pos, tube.num_triangle);
		lWalkthrough.pts = new Vector3f[lWalkthrough.num_pos];
		int j = 0; float za;
		for (za = start; za < end; za += gap){
			int num = 0;
			Vector3f sum;
			sum.set(0.0, 0.0, 0.0);

			for (unsigned i = 0; i < tube.num_triangle; i++){
				unsigned int a = tube.triangles[i].ver0, b = tube.triangles[i].ver1, c = tube.triangles[i].ver2;
				
				
			/*	if (j != 0 ){
					float cent[3] = { (tube.vers[a].pos[0] + tube.vers[b].pos[0] + tube.vers[c].pos[0]) / 3.0,
						(tube.vers[a].pos[1] + tube.vers[b].pos[1] + tube.vers[c].pos[1]) / 3.0,
						(tube.vers[a].pos[2] + tube.vers[b].pos[2] + tube.vers[c].pos[2]) / 3.0,
					};
					
					float cur_to_last_dist_sq = (last_point[0] - cent[0]) * (last_point[0] - cent[0])
						+ (last_point[1] - cent[1]) * (last_point[1] - cent[1]
						+ (last_point[2] - cent[2]) * (last_point[2] - cent[2]));

					if (cur_to_last_dist_sq > thrshd_sq){
						continue;
					}						
				}
			*/	
				
				p1 = tube.vers[a].pos; 
				p2 = tube.vers[b].pos; 

				if ((p1.z - za)*(p2.z - za) < 0.0) {
					u = (p1.z - za) / (p1.z - p2.z);
					p = p1 + Vector3f(u, u,u) * (p2 - p1);  
					// tmp.push_back(p);
					sum = sum + p;
					num++;
				}
				p1 = tube.vers[b].pos;
				p2 = tube.vers[c].pos;

				if ((p1.z - za)*(p2.z - za) < 0.0){
					u = (p1.z - za) / (p1.z - p2.z);
					p = p1 + Vector3f(u, u, u)*(p2 - p1);
					// tmp.push_back(p);
					sum = sum + p;
					num++;
				}

				p1 = tube.vers[c].pos; 
				p2 = tube.vers[a].pos; 
				if ((p1.z - za)*(p2.z - za) < 0.0){
					u = (p1.z - za) / (p1.z - p2.z);
					p = p1 + Vector3f(u, u, u)*(p2 - p1);
					// tmp.push_back(p);
					sum = sum + p;
					num++;
				}
			}

			if (num){

				// fprintf(stderr, "\n j = %d  num = %d  sum[0] = %f  sum[1] = %f sum[2] = %f", j, num, sum[0],sum[1],sum[2]);
				last_point = lWalkthrough.pts[j] =  Vector3f(.0, .0, .0) + (sum / Vector3f(num, num, num));
				j++;

				/*
				lWalkthrough.pts[3*j + 0] =  sum[0] ;
				lWalkthrough.pts[3*j + 1] =  sum[1] ;
				lWalkthrough.pts[3*j + 2] =  sum[2] ;
				*/
			}
			//lWalkthrough.push_back( sum/Vector3f(num));
			//lWalkthrough.push_back( Vector3f(.5,.0,.0) + sum/Vector3f(num));
		}

		// writing walk through points to file 
		lWalkthrough.num_pos = j;
		FILE *file_w = fopen("data_items/files/walk_through.gclff", "w");

		if (file_w)
		{
			fprintf(stderr, "\n ****center line is getting written to file***");
			fprintf(file_w, "%d \n", lWalkthrough.num_pos);
			for (unsigned i = 0; i < lWalkthrough.num_pos; i++)
				fprintf(file_w, "%f %f %f \n", lWalkthrough.pts[i].x, lWalkthrough.pts[i].y, lWalkthrough.pts[i].z);
			fclose(file_w);
		}
		fprintf(stderr, "\n ****center line calculation ends here***\n");

	}
	Point *pts = new Point[lWalkthrough.num_pos];
	for (int i = 0; i < lWalkthrough.num_pos; i++)
	{
		pts[i].x = 0.5 + lWalkthrough.pts[i].x;
		pts[i].y = lWalkthrough.pts[i].y;
		pts[i].z = lWalkthrough.pts[i].z;
	}

	smoothtube.initCurve(20, 4);
	smoothtube.lsApprox(lWalkthrough.num_pos - 1, pts);

	delete[] pts;
	return lWalkthrough;
}

graph find_graph(model *mod) {
	fprintf(stderr, "\n ****graph calcultaion starts here***");
	graph g;
	g.node_cnt = (*mod).num_triangle;
	g.mod = mod;
	g.nodes = new graph_node[g.node_cnt];
	
	FILE *file_r = fopen("data_items/files/mesh_graph.dgff", "rb");

	if (file_r)
	{
		int node_cnt;
		fprintf(stderr, "\n ****graph is getting read from file***");
		fscanf(file_r, "%d", &node_cnt);
		if (node_cnt == g.node_cnt){
			for (unsigned i = 0; i < node_cnt; i++){
				fscanf(file_r, "%d %d %d", &g.nodes[i].edge0, &g.nodes[i].edge1, &g.nodes[i].edge2);
			}

		}
		fclose(file_r);
		fprintf(stderr, "\n ****graph calculation ends here***");
		return g;
	}

	for (int i = 0; i < g.node_cnt; i++)
		g.nodes[i].edge0 = g.nodes[i].edge1 = g.nodes[i].edge2 = -1;

	// calculations for edgelist begins here*******************************

	std::vector<mod_edge> mod_edge_list;

	for (int i = 0; i < g.node_cnt; i++){
		int ver0 = mod->triangles[i].ver0;
		int ver1 = mod->triangles[i].ver1;
		int ver2 = mod->triangles[i].ver2;

		mod_edge me1(ver0, ver1, i);
		mod_edge me2(ver1, ver2, i);
		mod_edge me3(ver2, ver0, i);

		mod_edge_list.push_back(me1);
		mod_edge_list.push_back(me2);
		mod_edge_list.push_back(me3);
	}

	std::sort(mod_edge_list.begin(), mod_edge_list.end());

	for (unsigned k = 0; k < mod_edge_list.size() - 1; k++)
	{
		if (mod_edge_list[k].u_ver_idx == mod_edge_list[k + 1].u_ver_idx && mod_edge_list[k].v_ver_idx == mod_edge_list[k + 1].v_ver_idx)
		{
			int i = mod_edge_list[k].tri_idx;
			int j = mod_edge_list[k + 1].tri_idx;

			if (g.nodes[i].edge0 == -1)
				g.nodes[i].edge0 = j;
			else if (g.nodes[i].edge1 == -1)
				g.nodes[i].edge1 = j;
			else
				g.nodes[i].edge2 = j;


			if (g.nodes[j].edge0 == -1)
				g.nodes[j].edge0 = i;
			else if (g.nodes[j].edge1 == -1)
				g.nodes[j].edge1 = i;
			else
				g.nodes[j].edge2 = i;
		}
	}

	for (unsigned i = 0; i < 5; i++){
		fprintf(stderr, "%d %d %d \n", g.nodes[i].edge0, g.nodes[i].edge1, g.nodes[i].edge2);
	}

	for (unsigned i = g.node_cnt - 5; i < g.node_cnt; i++){
		fprintf(stderr, "%d %d %d \n", g.nodes[i].edge0, g.nodes[i].edge1, g.nodes[i].edge2);
	}

	FILE *file_w = fopen("data_items/files/mesh_graph.dgff", "w");

	if (file_w)
	{
		int node_cnt = g.node_cnt;
		fprintf(stderr, "\n ****graph is being written from file***");
		fprintf(file_w, "%d \n", node_cnt);

		for (unsigned i = 0; i < node_cnt; i++){
			fprintf(file_w, "%d %d %d \n", g.nodes[i].edge0, g.nodes[i].edge1, g.nodes[i].edge2);
		}
		fclose(file_w);

	}

	// calculations for edgelist ends here*******************************
	fprintf(stderr, "\n ****graph calcultaion ends here***\n");
	return g;
}

void debug_graph(graph g){
	printf("\n printing of graph begins \n");

	for (int i = 0; i < g.node_cnt; i++){
		printf("\n Triangle %d :: neighbours %d %d %d ", i, g.nodes[i].edge0, g.nodes[i].edge1, g.nodes[i].edge2);
	}
	printf("\n printing of graph ends \n");
}

int LapTexture::inclusionTest(int neigh){

	int neigh_ver0 = g.mod->triangles[neigh].ver0;
	int neigh_ver1 = g.mod->triangles[neigh].ver1;
	int neigh_ver2 = g.mod->triangles[neigh].ver2;

	Vector3f neigh_ver0_pos, neigh_ver1_pos, neigh_ver2_pos;
	Vector3f neigh_edge1, neigh_edge2, neigh_normal;

	neigh_ver0_pos = g.mod->vers[neigh_ver0].pos;
	neigh_ver1_pos = g.mod->vers[neigh_ver1].pos;
	neigh_ver2_pos = g.mod->vers[neigh_ver2].pos;

	neigh_edge1 = neigh_ver1_pos - neigh_ver0_pos;
	neigh_edge2 = neigh_ver2_pos - neigh_ver0_pos;

	neigh_normal = neigh_edge1 ^ neigh_edge2;
	neigh_normal = neigh_normal.normalize();

	return  acos(seed_normal & neigh_normal) < (3 * 3.14) / 8.0 ? 1 : -1;
}

void LapTexture::find_seed_normal_and_cen_proj()
{
	Vector3f z_axis;
	z_axis.set(0.0, 0.0, 1.0);
	int seed_ver0 = g.mod->triangles[seed].ver0;
	int seed_ver1 = g.mod->triangles[seed].ver1;
	int seed_ver2 = g.mod->triangles[seed].ver2;

	Vector3f seed_ver0_pos, seed_ver1_pos, seed_ver2_pos;
	Vector3f edge1, edge2;
	
	seed_ver0_pos = g.mod->vers[seed_ver0].pos;
	seed_ver1_pos = g.mod->vers[seed_ver1].pos;
	seed_ver2_pos = g.mod->vers[seed_ver2].pos;
	
	edge1 = seed_ver1_pos - seed_ver0_pos;
	edge2 = seed_ver2_pos - seed_ver0_pos;
	

	seed_normal = edge1 ^ edge2;
	seed_normal = seed_normal.normalize();

	float th = acos(seed_normal & z_axis); //dot product
	float ct = cos(th), rt = 1 - cos(th), st = sin(th);

	//printf("\n angle = %f", th);

	Vector3f u;
	u = seed_normal ^ z_axis;//cross product
	u = u.normalize();

	float rot[3][3] = { { ct + u.x * u.x * rt, u.x * u.y * rt - u.z * st, u.x * u.z * rt + u.y * st },
					    { u.y * u.x * rt + u.z * st, ct + u.y * u.y * rt, u.y * u.z * rt - u.x * st },
						{ u.z * u.x * rt - u.y * st, u.z * u.y * rt + u.x * st, ct + u.z * u.z * rt }
					  };

	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			rm[i][j] = rot[i][j];
		}
	}

	seed_cen_proj[0] = (
		(seed_ver0_pos.x * rm[0][0] + seed_ver0_pos.y * rm[0][1] + seed_ver0_pos.z * rm[0][2]) +
		(seed_ver1_pos.x * rm[0][0] + seed_ver1_pos.y * rm[0][1] + seed_ver1_pos.z * rm[0][2]) +
		(seed_ver2_pos.x * rm[0][0] + seed_ver2_pos.y * rm[0][1] + seed_ver2_pos.z * rm[0][2])
		) / 3.0;

	seed_cen_proj[1] = (
		(seed_ver0_pos.x * rm[1][0] + seed_ver0_pos.y * rm[1][1] + seed_ver0_pos.z * rm[1][2]) +
		(seed_ver1_pos.x * rm[1][0] + seed_ver1_pos.y * rm[1][1] + seed_ver1_pos.z * rm[1][2]) +
		(seed_ver2_pos.x * rm[1][0] + seed_ver2_pos.y * rm[1][1] + seed_ver2_pos.z * rm[1][2])
		) / 3.0;

}

void LapTexture::process_extended_patch_triangle(int neigh, std::vector<extend_patch_entry> &patch_border_list, int node) {
	
	if (neigh == -1)
		return;

	int adj_ver0 = g.mod->triangles[node].ver0, adj_ver1 = g.mod->triangles[node].ver1, adj_ver2 = g.mod->triangles[node].ver2;

	for (int i = 0; i < patch_border_list.size(); i++){
		if (patch_border_list[i].tri == neigh){
			 
			patch_border_list[i].blend[0] = patch_border_list[i].blend[1] = patch_border_list[i].blend[2] = 0.5;
			return;
		}
	}


	extend_patch_entry epe;
	epe.tri = neigh;
	epe.partial_patch_num = cur_patch_num;
	epe.blend[0] = epe.blend[1] = epe.blend[2] = 0.5;

	int node_ver;
	Vector3f node_ver_pos;
	float texture_scalling = 1.0f;
	
	node_ver = g.mod->triangles[neigh].ver0;
	node_ver_pos = g.mod->vers[node_ver].pos;
	
	epe.uv[0].x = 0.5 + texture_scalling * ((node_ver_pos.x * rm[0][0] + node_ver_pos.y * rm[0][1] + node_ver_pos.z * rm[0][2]) - seed_cen_proj[0]);
	epe.uv[0].y = 0.5 + texture_scalling * ((node_ver_pos.x * rm[1][0] + node_ver_pos.y * rm[1][1] + node_ver_pos.z * rm[1][2]) - seed_cen_proj[1]);
	if (node_ver != adj_ver0 && node_ver != adj_ver1 && node_ver != adj_ver2)
		epe.blend[0] = 3.0;


	node_ver = g.mod->triangles[neigh].ver1;

	node_ver_pos = g.mod->vers[node_ver].pos;

	epe.uv[1].x = 0.5 + texture_scalling * ((node_ver_pos.x * rm[0][0] + node_ver_pos.y * rm[0][1] + node_ver_pos.z * rm[0][2]) - seed_cen_proj[0]);
	epe.uv[1].y = 0.5 + texture_scalling * ((node_ver_pos.x * rm[1][0] + node_ver_pos.y * rm[1][1] + node_ver_pos.z * rm[1][2]) - seed_cen_proj[1]);
	if (node_ver != adj_ver0 && node_ver != adj_ver1 && node_ver != adj_ver2)
		epe.blend[1] = 3.0;

	node_ver = g.mod->triangles[neigh].ver2;

	node_ver_pos = g.mod->vers[node_ver].pos;

	epe.uv[2].x = 0.5 + texture_scalling * ((node_ver_pos.x * rm[0][0] + node_ver_pos.y * rm[0][1] + node_ver_pos.z * rm[0][2]) - seed_cen_proj[0]);
	epe.uv[2].y = 0.5 + texture_scalling * ((node_ver_pos.x * rm[1][0] + node_ver_pos.y * rm[1][1] + node_ver_pos.z * rm[1][2]) - seed_cen_proj[1]);
	if (node_ver != adj_ver0 && node_ver != adj_ver1 && node_ver != adj_ver2)
		epe.blend[2] = 3.0;

	patch_border_list.push_back(epe);
}

void LapTexture::process_neigh(int node, int *cur_dist, int neigh, std::vector<extend_patch_entry> &patch_border_list)
{
	if (neigh != -1 && isCovered[neigh] == 0){
		if (inclusionTest(neigh) == 1)
		{
			bfs_q.push(neigh);
			isCovered[neigh] = 1;
			covering_patch[neigh] = cur_patch_num;
			bfsDist[neigh] = *cur_dist + 1;
			cur_patch_size++;
			if (isCircular == 0)
				distortion = bfsDist[neigh] - circularDist;
		}

		else
		{
			if (isCircular == 1) {
				isCircular = 0;
				circularDist = *cur_dist;
			}
			process_extended_patch_triangle(neigh, patch_border_list, node);
		}
	}
	else if (neigh != -1 && (isCovered[neigh] == 1) && covering_patch[neigh] != cur_patch_num){
		process_extended_patch_triangle(neigh, patch_border_list, node);
	}
	
}

void LapTexture::process_neighbours(int node, int *cur_dist, std::vector<extend_patch_entry> &patch_border_list)
{
	int neigh0 = g.nodes[node].edge0;
	int neigh1 = g.nodes[node].edge1;
	int neigh2 = g.nodes[node].edge2;

	if (distortion <= DISTORTION_TOL_TEXTURE){
		process_neigh(node, cur_dist, neigh0, patch_border_list);
		process_neigh(node, cur_dist, neigh1, patch_border_list);
		process_neigh(node, cur_dist, neigh2, patch_border_list);
	}

	else{
		if (covering_patch[neigh0] != cur_patch_num){
			process_extended_patch_triangle(neigh0, patch_border_list, node);
		}

		if (covering_patch[neigh1] != cur_patch_num){
			process_extended_patch_triangle(neigh1, patch_border_list, node);
		}

		if (covering_patch[neigh2] != cur_patch_num){
			process_extended_patch_triangle(neigh2, patch_border_list, node);
		}
	}
}

void LapTexture::process_node_vertex(int node, int node_ver, int ver_num, std::vector<dup_ver_info> &dup_ver_cur_patch)
{
	
	Vector3f node_ver_pos;
	node_ver_pos = g.mod->vers[node_ver].pos;
	
	float texture_scalling = 1.0f;
	float uv0 = 0.5 + texture_scalling * ((node_ver_pos.x * rm[0][0] + node_ver_pos.y * rm[0][1] + node_ver_pos.z * rm[0][2]) - seed_cen_proj[0]);
	float uv1 = 0.5 + texture_scalling * ((node_ver_pos.x * rm[1][0] + node_ver_pos.y * rm[1][1] + node_ver_pos.z * rm[1][2]) - seed_cen_proj[1]);

	
	if (patchNum[node_ver] != -1 && patchNum[node_ver] != cur_patch_num){
		// duplicate vertex

		//check whether already duplicated in current patch
		
		int indx_of_already_duplicated = -1;
		for (int i = 0; i < dup_ver_cnt_cur_patch; i++){
			if (dup_ver_cur_patch[i].similar_ver == node_ver){
				indx_of_already_duplicated = i;
				break;
			}
		}

		if (indx_of_already_duplicated != -1){
			already_dup_ver_info adv;
			adv.triangle = node;
			adv.triangle_ver_num = ver_num;
			adv.already_dup_ver = g.mod->num_vert + dup_ver_cnt + indx_of_already_duplicated;
			already_dup_ver.push_back(adv);
		}
		else{
			dup_ver_info new_ver;
			new_ver.similar_ver = node_ver;
			new_ver.u = uv0;
			new_ver.v = uv1;
			new_ver.which_triangle = node;
			new_ver.triangle_ver_num = ver_num;

			new_ver.patch_col = cur_patch_col;
			//new_ver.is_mucous_present = is_mucous_present;
			dup_ver_cur_patch.push_back(new_ver);
			dup_ver_cnt_cur_patch++;
			//g.mod->indx[3 * node + 0] = g.mod->num_vert + dup_ver_cnt - 1;
		}
		
	}
	else{
		g.mod->vers[node_ver].uv_texture.set(uv0, uv1);
		g.mod->vers[node_ver].patch_col_texture = cur_patch_col;
		g.mod->vers[node_ver].is_mucous_present = is_mucous_present;
		patchNum[node_ver] = cur_patch_num;
	}
}

void LapTexture::process_node(int node, std::vector<dup_ver_info> &dup_ver_cur_patch)
{
	int node_ver0 = g.mod->triangles[node].ver0;
	int node_ver1 = g.mod->triangles[node].ver1;
	int node_ver2 = g.mod->triangles[node].ver2;

	process_node_vertex(node, node_ver0, 0, dup_ver_cur_patch);
	process_node_vertex(node, node_ver1, 1, dup_ver_cur_patch);
	process_node_vertex(node, node_ver2, 2, dup_ver_cur_patch);
}

void LapTexture::make_vertex_copy(ver *verts, int i, int sim_ver){

	verts[i].pos = verts[sim_ver].pos;			
	verts[i].nor = verts[sim_ver].nor;			
	verts[i].tangent = verts[sim_ver].tangent;		
	verts[i].uv_texture = verts[sim_ver].uv_texture;	
	verts[i].is_mucous_present = verts[sim_ver].is_mucous_present;
	verts[i].patch_col_texture = Vector3f(0.75, 0.75, 0.75) * verts[sim_ver].patch_col_texture;
}

void LapTexture::adding_dup()
{
	ver *verts;

	cal_tangent( *(g.mod));

	verts = new ver[g.mod->num_vert + dup_ver_cnt + 3 * patch_cnt +  3 * border_list.size()];

	for (unsigned i = 0; i < g.mod->num_vert; i++)
	{
		verts[i].is_partial_patch = 0;
		verts[i].pos = g.mod->vers[i].pos; 
		verts[i].nor = g.mod->vers[i].nor;
		verts[i].tangent = g.mod->vers[i].tangent;
		verts[i].uv_texture = g.mod->vers[i].uv_texture;
		verts[i].patch_col_texture = g.mod->vers[i].patch_col_texture;
		verts[i].is_mucous_present = g.mod->vers[i].is_mucous_present;
	}

	for (unsigned i = g.mod->num_vert, j = 0; i < g.mod->num_vert + dup_ver_cnt; i++, j++)
	{
		dup_ver_info new_ver = dup_ver[j];
		verts[i].is_partial_patch = 0;
		int similar_ver = new_ver.similar_ver;
		float u = new_ver.u, v = new_ver.v;
		int which_triangle = new_ver.which_triangle;
		int triangle_ver_num = new_ver.triangle_ver_num;

		verts[i].pos = g.mod->vers[similar_ver].pos;         
		verts[i].nor = g.mod->vers[similar_ver].nor;        
		verts[i].tangent = g.mod->vers[similar_ver].tangent;
		verts[i].is_mucous_present = g.mod->vers[similar_ver].is_mucous_present;

		verts[i].uv_texture.set(u, v);
		verts[i].patch_col_texture = new_ver.patch_col;  			
				
		switch (triangle_ver_num)
		{
			case 0:
				g.mod->triangles[which_triangle].ver0 = i;
				break;
			case 1:
				g.mod->triangles[which_triangle].ver1 = i;
				break;
			case 2:
				g.mod->triangles[which_triangle].ver2 = i;
				break;
		}
	}

	for (unsigned i = 0; i < already_dup_ver.size(); i++)
	{
		switch (already_dup_ver[i].triangle_ver_num)
		{
			case 0:
				g.mod->triangles[already_dup_ver[i].triangle].ver0 = already_dup_ver[i].already_dup_ver ;
				break;																					
			case 1:																						
				g.mod->triangles[already_dup_ver[i].triangle].ver1 = already_dup_ver[i].already_dup_ver ;
				break;																					
			case 2:																						
				g.mod->triangles[already_dup_ver[i].triangle].ver2 = already_dup_ver[i].already_dup_ver ;
				break;
		}
	}


	for (int i = g.mod->num_vert + dup_ver_cnt, j = 1; i < g.mod->num_vert + dup_ver_cnt + 3 * patch_cnt; j++)
	{
		for (int k = 0; k < 3; k++)
		{
			int sim_ver;
			//printf("printing ver %d, %d, %d", g.mod->triangles[patches[j].seed_trianlge].ver0, g.mod->triangles[patches[j].seed_trianlge].ver1, g.mod->triangles[patches[j].seed_trianlge].ver2);
			switch (k)
			{
				case 0:
					sim_ver = g.mod->triangles[patches[j].seed_trianlge].ver0;
					g.mod->triangles[patches[j].seed_trianlge].ver0 = i;
					break;
				case 1:
					sim_ver = g.mod->triangles[patches[j].seed_trianlge].ver1;
					g.mod->triangles[patches[j].seed_trianlge].ver1 = i;
					break;
				case 2:
					sim_ver = g.mod->triangles[patches[j].seed_trianlge].ver2;
					g.mod->triangles[patches[j].seed_trianlge].ver2 = i;
					break;
			}
			
			verts[i].is_partial_patch = 0;
			verts[i].pos			= verts[sim_ver].pos;	
			verts[i].nor			= verts[sim_ver].nor;
			verts[i].tangent		= verts[sim_ver].tangent;
			verts[i].uv_texture = verts[sim_ver].uv_texture;
			verts[i].patch_col_texture.set(0, 0, 0);
			verts[i].is_mucous_present = verts[sim_ver].is_mucous_present;
			i++;
		}
	}
	
	for (int j = 0,  i = g.mod->num_vert + dup_ver_cnt + 3 * patch_cnt; i < g.mod->num_vert + dup_ver_cnt + 3 * patch_cnt + 3 * border_list.size();)
	{
		extend_patch_entry epe = border_list[j];  j++;
		make_vertex_copy(verts, i, g.mod->triangles[epe.tri].ver0);  
		verts[i].is_partial_patch = 10;
		verts[i].bound_uv_texture1 = epe.uv[0];
		verts[i].partial_blending = epe.blend[0];
		g.mod->triangles[epe.tri].ver0 = i;
		i++;
		
		make_vertex_copy(verts, i, g.mod->triangles[epe.tri].ver1);
		verts[i].is_partial_patch = 10;
		verts[i].bound_uv_texture1 = epe.uv[1];
		verts[i].partial_blending = epe.blend[1];
		g.mod->triangles[epe.tri].ver1 = i;
		i++;

		make_vertex_copy(verts, i, g.mod->triangles[epe.tri].ver2);
		verts[i].is_partial_patch = 10;
		verts[i].bound_uv_texture1 = epe.uv[2];
		verts[i].partial_blending = epe.blend[2];
		g.mod->triangles[epe.tri].ver2 = i;
		i++;
	}
	g.mod->num_vert = g.mod->num_vert + dup_ver_cnt + 3 *patch_cnt + 3 * border_list.size();  
	delete g.mod->vers;
	g.mod->vers = verts;
}

void LapTexture::find_adj_patches(int tri, int *adj_patch0, int *adj_patch1, int *adj_patch2) {

	int valid_patch;

	if (g.nodes[tri].edge0 != -1)
			valid_patch = covering_patch[g.nodes[tri].edge0];
	else if (g.nodes[tri].edge1 != -1)
			valid_patch = covering_patch[g.nodes[tri].edge1];
	else
			valid_patch = covering_patch[g.nodes[tri].edge2];
	

	if (g.nodes[tri].edge0 == -1)
		*adj_patch0 = valid_patch;
	else
		*adj_patch0 = covering_patch[g.nodes[tri].edge0];

	
	if (g.nodes[tri].edge1 == -1)
		*adj_patch1 = valid_patch;
	else
		*adj_patch1 = covering_patch[g.nodes[tri].edge1];
	
	
	if (g.nodes[tri].edge2 == -1)
		*adj_patch2 = valid_patch;
	else
		*adj_patch2 = covering_patch[g.nodes[tri].edge2];

}

int LapTexture::find_suitable_patch_for_merging(int *adj_patch0, int *adj_patch1, int *adj_patch2)
{
	int max_triangle_patch = max_int(patches[*adj_patch0].triangle_count, patches[*adj_patch1].triangle_count, patches[*adj_patch2].triangle_count);

	int patch_for_merging;

	if (max_triangle_patch == patches[*adj_patch0].triangle_count)
	{
		patch_for_merging = *adj_patch0;
	}
	else if (max_triangle_patch == patches[*adj_patch1].triangle_count)
	{
		patch_for_merging = *adj_patch1;
	}
	else{
		patch_for_merging = *adj_patch2;
	}
	return patch_for_merging;
}

void LapTexture::merging_single_triangles()
{
	for (unsigned i = 0; i < single_size_patch.size(); i++){
		int tri = patches[single_size_patch[i]].seed_trianlge;
		int adj_patch0, adj_patch1, adj_patch2;
		find_adj_patches(tri, &adj_patch0, &adj_patch1, &adj_patch2);
		int patch_for_merging = find_suitable_patch_for_merging(&adj_patch0, &adj_patch1, &adj_patch2);
		if (patches[patch_for_merging].triangle_count == 1)
			continue;
		patch_freeq[patches[patch_for_merging].triangle_count]--;
		patch_freeq[1]--;
		patch_freeq[patches[patch_for_merging].triangle_count + 1]++;
		//patches[patch_for_merging].triangle_count++;
		//if (patches[patch_for_merging].triangle_count > max_patch_size){
		//	max_patch_size = patches[patch_for_merging].triangle_count;
		//}

		seed = patches[patch_for_merging].seed_trianlge;
		
		find_seed_normal_and_cen_proj();

		int node_ver[3] = { g.mod->triangles[tri].ver0, g.mod->triangles[tri].ver1, g.mod->triangles[tri].ver2 };
		int seed_ver[3] = { g.mod->triangles[seed].ver0, g.mod->triangles[seed].ver1, g.mod->triangles[seed].ver2 };
		float texture_scalling = 1.0f;
		// assign texture coordinates

		for (int i = 0; i < 3; i++){
			Vector3f node_ver_pos;
			node_ver_pos = g.mod->vers[node_ver[i]].pos;
			float uv0 = 0.5 + texture_scalling * ((node_ver_pos.x * rm[0][0] + node_ver_pos.y * rm[0][1] + node_ver_pos.z * rm[0][2]) - seed_cen_proj[0]);
			float uv1 = 0.5 + texture_scalling * ((node_ver_pos.x * rm[1][0] + node_ver_pos.y * rm[1][1] + node_ver_pos.z * rm[1][2]) - seed_cen_proj[1]);
				
			g.mod->vers[node_ver[i]].uv_texture.set(uv0, uv1);
			g.mod->vers[node_ver[i]].patch_col_texture = patches[patch_for_merging].patch_col;
			
			
		}
	}
}

graph LapTexture::assign_laptexture()
{
	for (int i = 0; i < 30; i++){
		for (int j = 0; j < 30; j++){
			for (int k = 0; k < 30; k++){
				available_patch_col[30 * 30 * i + 30 * j + k].set(8 * i, 8 * j, 8 * k);
			}
		}
	}

	printf("\n bfs of graph begins \n");

	for (int i = 0; i < g.node_cnt; i++){
		isCovered[i] = 0;
		bfsDist[i] = -1;
		random_array[i] = i;
		covering_patch[i] = -1;
	}
		
	for (unsigned i = 0; i < g.mod->num_vert; i++){
		patchNum[i] = -1;
		ver_share_cnt[i] = 0;
	}
		
	for (int i = 0; i < 100000; i++){
		patch_freeq[i] = 0;
	}
	
	for (int i = 0; i < 100000; i++){
		patches[i].seed_trianlge = -1;
		patches[i].triangle_count = 0;
	}

	randomize(random_array, g.node_cnt);

	for (int i = 0; i < g.node_cnt; i++)
	{
		if (isCovered[random_array[i]] == 0)
		{  // valid seed 
			seed = random_array[i];
			patch_cnt++;
			cur_patch_size = 0;
			cur_patch_num = patch_cnt;
			//printf("\n seed = %d \n", seed);
				
			find_seed_normal_and_cen_proj();
			bfs_q.push(seed);
			isCovered[seed] = 1;
			bfsDist[seed] = 0;
			covering_patch[seed] = cur_patch_num;
			cur_patch_size++;
			//fprintf(stderr, "\nstarting bfs\n");
			distortion = 0;
			isCircular = 1;
			circularDist = 0;

			std::vector<dup_ver_info> dup_ver_cur_patch;
			std::vector<extend_patch_entry> patch_border_list;
			dup_ver_cnt_cur_patch = 0;
			is_mucous_present =   (((rand() % 12) > 8 ) ? 0.7 : 0.0 ) ;
			
			cur_patch_col = available_patch_col[cur_patch_num] / Vector3f(240.0, 240.0, 240.0);


			while (!bfs_q.empty()){
				int node = bfs_q.front();
				//float node_pos[3];
				int cur_dist = bfsDist[node];
				process_neighbours(node, &cur_dist, patch_border_list);
				process_node(node, dup_ver_cur_patch);
				bfs_q.pop();
			}
			
			for (int i = 0; i < dup_ver_cnt_cur_patch; i++)
				dup_ver.push_back(dup_ver_cur_patch[i]);


			for (int i = 0; i < patch_border_list.size(); i++)
				border_list.push_back(patch_border_list[i]);


			dup_ver_cnt += dup_ver_cnt_cur_patch;
			
			patches[cur_patch_num].seed_trianlge = seed;
			patches[cur_patch_num].triangle_count = cur_patch_size;
			patches[cur_patch_num].patch_col = cur_patch_col;
		
			patches[cur_patch_num].is_mucous_present = is_mucous_present;
			
			//patch_freeq[cur_patch_size]++;

			if (cur_patch_size == 1)
				single_size_patch.push_back(cur_patch_num);
			
			if (cur_patch_size > max_patch_size){
				max_patch_size = cur_patch_size;
			}

			if (cur_patch_size < min_patch_size){
				min_patch_size = cur_patch_size;
			}

			//printf("\n bfs of graph ends with node = %d visited \n", cnt);
		}
	}

	adding_dup();
		
	int tot_patch_cnt_tmp = 0;
	int diff_patch_size_cnt = 0;

	/*
	FILE *file_w = fopen("circular_change_patch_size_vs_freeq_before_merge.csv", "w");
	if (file_w)
	{
		for (int i = 1; i <= max_patch_size; i++){
			fprintf(file_w, "%d \n", patch_freeq[i]);
			if (patch_freeq[i] != 0)
			{
				diff_patch_size_cnt++;
				//printf("pf:%d:%d  ", i, patch_freeq[i]);
				tot_patch_cnt_tmp += patch_freeq[i];
			}
		}
		fclose(file_w);
	}
	*/
	printf("\n bfs of graph ends with patch_cnt = %d,  max patch size = %d min patch size = %d dup_ver_cnt = %d diff_patch_size_cnt = %d tot_patch_cnt_tmp = %d\n", patch_cnt, max_patch_size, min_patch_size, dup_ver_cnt, diff_patch_size_cnt, tot_patch_cnt_tmp);
	//for (int i = 0; i < 5000; i++){
	//	temp_patch_freeq[i] = patch_freeq[i];
	//}
	merging_single_triangles();
	
	/*file_w = fopen("circular_change_patch_size_vs_freeq_after_merge.csv", "w");
	printf("\n after patch merging\n");
	tot_patch_cnt_tmp = 0;
	diff_patch_size_cnt = 0;
	
	if (file_w)
	{
		for (int i = 1; i <= max_patch_size; i++){
			fprintf(file_w, "%d \n", patch_freeq[i]);
			if (patch_freeq[i] != 0)
			{
				diff_patch_size_cnt++;
				//printf("pf:%d:%d  ", i, patch_freeq[i]);
				tot_patch_cnt_tmp += patch_freeq[i];
			}
		}
		fclose(file_w);
	}
	*/
	printf("\n bfs of graph ends with patch_cnt = %d, max patch size = %d min patch size = %d dup_ver_cnt = %d diff_patch_size_cnt = %d tot_patch_cnt_tmp = %d\n", patch_cnt, max_patch_size, min_patch_size, dup_ver_cnt, diff_patch_size_cnt, tot_patch_cnt_tmp);

	return g;
}

LapTexture::LapTexture(graph input_g){
	g = input_g;
	dup_ver_cnt = 0;
	patches = new patch_info[100000];
	isCovered = new int[g.node_cnt];
	random_array = new int[g.node_cnt];
	bfsDist = new int[g.node_cnt];
	patchNum = new int[g.mod->num_vert];
	covering_patch = new int[g.node_cnt];
	ver_share_cnt = new int[g.mod->num_vert];
	patch_size = 30;
	min_patch_size = 10000000;
	max_patch_size = 0;
	patch_cnt = 0;
}