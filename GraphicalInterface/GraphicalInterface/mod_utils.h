/*
File : mod_utils.h
Author : Tarun Bansal
Description : functions related to model
*/

#ifndef MOD_UTILS_H
#define MOD_UTILS_H

#include "utils.h"
#define DISTORTION_TOL_TEXTURE 2
#define DISTORTION_TOL_MUCOUS 4
#define TEXTURE 1
#define MUCOUS 2

// for maintaing the information of nieghbouring trinalges who will have two set of texture coordinates for blending. Remember overcomming straight line artifacts..!!
typedef struct extend_patch_entry {
	int tri;
	int partial_patch_num;
	Vector2f uv[3];
	float blend[3];
} extend_patch_entry;

// For maintaing information of all vertex who will get duplicated.
typedef struct dup_ver_info {
	int similar_ver;
	float u, v;
	int which_triangle;
	int triangle_ver_num;
	Vector3f patch_col;
	float is_mucous_present;
} dup_ver_info;

// For maintaing information of vertices who have already got duplicated
typedef struct already_dup_ver_info
{
	int triangle;
	int triangle_ver_num;
	int already_dup_ver;
} already_dup_ver_info;


// Information of a patch
typedef struct patch_info{
	int seed_trianlge;
	int triangle_count;
	Vector3f patch_col;
	float is_mucous_present;
} patch_info;

// For loading .off file
model loadOFF(const char *offfile);
// For generating tangents for all vertices of model. These tangents will be used in rotation matrix in fragment shader.
void cal_tangent(model mod);
// For calculating centerpoint in GI tract at GAP distance
cam_path makeCenterLine(const  model &tube, float start, float end, float gap, BSpline &smoothtube);
//Method for finding dual graph
graph find_graph(model *mod);
//Method for debugging a dual graph
void debug_graph(graph g);


class LapTexture{
public:
	graph g;
	std::queue<int> bfs_q;                             // For doing BFS for patch growing
	std::vector<dup_ver_info> dup_ver;                 // List of duplicated vertices 
	std::vector<already_dup_ver_info> already_dup_ver; // Vertices already have been duplicated and occuring again for duplication
	std::vector<int> single_size_patch;                // List of patches having one triangle
	std::vector<extend_patch_entry> border_list;	   // List of bordering triangles 
													
	int dup_ver_cnt;								   // Total vertices that have been duplicated
	patch_info *patches;							   // Info of all patches
	int *isCovered;                                    // For keeping track of covered triangles is patches
	int *random_array;								   // For random seed selection
	int *bfsDist;									   // BFS distance of a triangle from seed triangle of 	correspond patch
	int *patchNum;									   // Current growing patch
	int *covering_patch;							   // Triangle covered by which patch
	int *ver_share_cnt;								   // How many triangles are sharing a vertex
	int patch_size;									   // Patch size of current patch
	int min_patch_size;								   // For debugging
	int max_patch_size;								   // For debugging
	int patch_cnt;									   // How many patches
	int patch_freeq[100000];						   // For debugging
	Vector3f available_patch_col[27000];			   // List of colours to be assigned to each patch
	

	// attirbutes for current patch that is being calculated

	int seed;										  // Seed for current patch
	int cur_patch_size;								  // Size of current patch
	int cur_patch_num;								  // Patch number of current patch
	float seed_cen_proj[2];                           // projection of centroid on 2d plane of seed triangle
	float rm[3][3];		  							  // Rotation matrix for current patch
	Vector3f seed_normal;							  // Normal of seed of current patch
	int distortion;									  // Distortion in current patch
	int isCircular;									  // If current patch circular
	int circularDist;								  // Circular radius of currnet patch
	int dup_ver_cnt_cur_patch;						  // total number vertices got duplicated while growing current patch
	float is_mucous_present;						  // Whether patch contains mucous
	Vector3f cur_patch_col;							  // Colour given to current patch under consideration

	void find_seed_normal_and_cen_proj();
	int inclusionTest(int neigh);
	void process_neighbours(int node, int *cur_dist, std::vector<extend_patch_entry> &patch_border_list);
	void process_node(int node, std::vector<dup_ver_info> &dup_ver_cur_patch);
	void process_neigh(int node, int *cur_dist,  int neigh, std::vector<extend_patch_entry> &patch_border_list);
	void process_extended_patch_triangle(int neigh, std::vector<extend_patch_entry> &patch_border_list, int node);
	void process_node_vertex(int node, int node_ver, int ver_num, std::vector<dup_ver_info> &dup_ver_cur_patch);
	void make_vertex_copy(ver *verts, int i, int sim_ver);
	void adding_dup();
	void merging_single_triangles();
	void find_adj_patches(int tri, int *adj_patch0, int *adj_patch1, int *adj_patch2);
	int find_suitable_patch_for_merging(int *adj_patch0, int *adj_patch1, int *adj_patch2);
	graph assign_laptexture();

	LapTexture(graph input_g);
};

#endif