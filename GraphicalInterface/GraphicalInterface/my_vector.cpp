/*
File : my_vector.cpp
Author : Content of this file was written by Rahul. I(Tarun Bansal) am reusing it.
Description : Fuctions for vectors.
*/

#include "my_vector.h"
#include <math.h>
Vector2f::Vector2f(){
	x = y = 0.0;
}

Vector2f::Vector2f(float a){
	x = y = a;
}

Vector2f::Vector2f(float a, float b){
	x = a;
	y = b;
}

void Vector2f::set(float a, float b){
	x = a;
	y = b;
}

Vector2f& Vector2f::operator = (const Vector2f &a){
	x = a.x;
	y = a.y;
	return *this;
}

Vector2f operator + (const Vector2f &a, const Vector2f &b){
	Vector2f c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	return c;
}

Vector2f operator - (const Vector2f &a, const Vector2f &b){
	Vector2f c;
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	return c;
}

Vector2f operator * (const Vector2f &a, const Vector2f &b){
	Vector2f c;
	c.x = a.x * b.x;
	c.y = a.y * b.y;
	return c;
}

Vector2f operator / (const Vector2f &a, const Vector2f &b){
	Vector2f c;
	c.x = a.x / b.x;
	c.y = a.y / b.y;
	return c;
}

Vector2f& Vector2f::operator += (const float d){
	x += d;
	y += d;
	return *this;
}

Vector2f& Vector2f::operator += (const Vector2f &d){
	x += d.x;
	y += d.y;
	return *this;
}


Vector3f::Vector3f(){
	x = y = z = 0.0;
}

Vector3f::Vector3f(float a){
	x = y = z = a;
}

Vector3f::Vector3f(float a, float b, float c){
	x = a;
	y = b;
	z = c;
}

void Vector3f::set(float a, float b, float c){
	x = a;
	y = b;
	z = c;
}

Vector3f& Vector3f::operator = (const Vector3f &a){
	x = a.x;
	y = a.y;
	z = a.z;
	return *this;
}

Vector3f Vector3f::operator + (){
	return *this;
}

Vector3f Vector3f::operator - (){
	Vector3f a;
	a.x = -x;
	a.y = -y;
	a.z = -z;
	return a;
}

Vector3f& Vector3f::operator += (const Vector3f &d){
	x += d.x;
	y += d.y;
	z += d.z;
	return *this;
}

Vector3f& Vector3f::operator -= (const Vector3f &d){
	x -= d.x;
	y -= d.y;
	z -= d.z;
	return *this;
}

Vector3f& Vector3f::operator *= (const Vector3f &d){
	x *= d.x;
	y *= d.y;
	z *= d.z;
	return *this;
}

Vector3f& Vector3f::operator /= (const Vector3f &d){
	x /= d.x;
	y /= d.y;
	z /= d.z;
	return *this;
}

Vector3f operator + (const Vector3f &a, const Vector3f &b){
	Vector3f c;
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
	return c;
}

Vector3f operator - (const Vector3f &a, const Vector3f &b){
	Vector3f c;
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
	return c;
}

float operator & (const Vector3f &a, const Vector3f &b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vector3f operator * (const Vector3f &a, const Vector3f &b){
	Vector3f c;
	c.x = a.x * b.x;
	c.y = a.y * b.y;
	c.z = a.z * b.z;
	return c;
}

Vector3f operator ^ (const Vector3f &a, const Vector3f &b){
	Vector3f c;
	c.x = a.y*b.z - a.z*b.y;
	c.y = a.z*b.x - a.x*b.z;
	c.z = a.x*b.y - a.y*b.x;
	return c;
}

Vector3f operator / (const Vector3f &a, const Vector3f &b){
	Vector3f c;
	c.x = a.x / b.x;
	c.y = a.y / b.y;
	c.z = a.z / b.z;
	return c;
}

float Vector3f::magnitude()const{
	return sqrt(x*x + y*y + z*z);
}

Vector3f Vector3f::normalize()const{
	float m = magnitude();
	if (m > .0000001)
	{
		return Vector3f(x / m, y / m, z / m);
	}
	return Vector3f(x, y, z);
	
}


