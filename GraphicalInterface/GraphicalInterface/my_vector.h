/*
File : my_vector.h
Author : Content of this file was written by Rahul. I(Tarun Bansal) am reusing it.
Description : Fuctions for vectors.
*/


#ifndef MY_VECTOR_H
#define MY_VECTOR_H

class Vector2f{
public:
	float x;
	float y;
	Vector2f();
	Vector2f(float a);
	Vector2f(float a, float b);
	void set(float a, float b);
	//-- unary
	//Vector2f operator + ();
	//Vector2f operator - ();
	//--
	Vector2f& operator = (const Vector2f &a);
	friend Vector2f operator + (const Vector2f &a, const Vector2f &b);
	friend Vector2f operator - (const Vector2f &a, const Vector2f &b);
	friend Vector2f operator * (const Vector2f &a, const Vector2f &b);
	friend Vector2f operator / (const Vector2f &a, const Vector2f &b);
	Vector2f& operator += (const float d);
	Vector2f& operator += (const Vector2f &d);
};

class Vector3f{
public:
	float x;
	float y;
	float z;
	Vector3f();
	Vector3f(float a);
	Vector3f(float a, float b, float c);

	void set(float a, float b, float c);
	Vector3f& operator = (const Vector3f &a);
	Vector3f  operator + ();
	Vector3f  operator - ();
	Vector3f& operator += (const Vector3f &d);	// element by element addition
	Vector3f& operator -= (const Vector3f &d);	// element by element addition
	Vector3f& operator *= (const Vector3f &d);	// element by element multiplication
	Vector3f& operator /= (const Vector3f &d);	// element by element multiplication
	friend Vector3f operator + (const Vector3f &a, const Vector3f &b);	// element by element addition
	friend Vector3f operator - (const Vector3f &a, const Vector3f &b);	// element by element subtraction
	friend float operator & (const Vector3f &a, const Vector3f &b);	// dot product
	friend Vector3f operator * (const Vector3f &a, const Vector3f &b);	// element by element multiplication
	friend Vector3f operator ^ (const Vector3f &a, const Vector3f &b);	// cross product
	friend Vector3f operator / (const Vector3f &a, const Vector3f &b);	// division
	float distance(const Vector3f& a)const;
	float magnitude() const;
	Vector3f normalize()const;
	Vector3f rotate(float xy, float yz, float zx)const;
};


#endif
