//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//		Note  ----  Normalize after extracting x,y,z components of a vector, don't normalize directly 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#version 330

in vec2 ex_uv;   
in vec4 point_pos_camera;              
in vec3 ex_nor;                    // in eye space     
in vec3 ex_tan_vec;               // tangent vector in eye space
in vec3 light_dir_for_dirLS;
in vec4 trans_light_pos;
in vec3 point_pos_model;  
in vec3 ex_patch_col_texture;
in vec2 ex_bound_uv_texture1;
in float  ex_is_partial_patch;
in float ex_partial_blending;
in float ex_is_mucous_present;

layout (location = 0) out vec4 depth;
layout (location = 1) out vec4 color;

uniform sampler2D texture;               // tissue texture
uniform sampler2D ref_map;               // reflectance map
uniform sampler2D gloss_map;               // gloss map

uniform sampler2D texture_tmp;               // tissue texture
uniform sampler2D ref_map_tmp;               // reflectance map
uniform sampler2D gloss_map_tmp;               // gloss map
uniform sampler2D mucous;               // mucous texture 1
uniform sampler2D mucous_tmp;               // mucous texture 2

uniform int which_texture;
uniform int which_ref_map;
uniform int which_gloss_map;
uniform int which_mucous;

uniform vec3 light_col;                 // incident light color 
uniform vec3 amb_light_col;             // ambient light color
uniform int shiny;                    // shininess of surface
uniform int is_normal_mapping;
uniform int is_gloss_mapping;
uniform int is_invert_narmal;
uniform int is_point_light;
uniform int is_diffuse;
uniform int is_specular;
uniform int is_mucous;
uniform int show_patch;
uniform float cons_atten;    // constant term in attenuation
uniform float lin_atten;     // linear coff in attenuation
uniform float qd_atten;      // quadratic coff in attenuation
uniform int is_partial_blending;  


vec3 cal_perturbed_nor()
{	vec2 new_ex_uv = vec2(ex_uv.x + .1 , ex_uv.y + .1);	
    vec3 Normal = normalize(ex_nor);
    vec3 Tangent = normalize(ex_tan_vec);
    Tangent = normalize(Tangent - dot(Tangent, Normal) * Normal); // Gram-Schmidt process
    
	vec3 Bitangent = cross(Tangent, Normal);

	//Tangent = cross(Normal, Bitangent);

    vec3 BumpMapNormal = texture2D(ref_map, ex_uv).xyz;
	if(which_ref_map == 1)
	{
		BumpMapNormal = texture2D(ref_map_tmp, ex_uv).xyz;
	}
    BumpMapNormal = 2.0 * BumpMapNormal - vec3(1.0, 1.0, 1.0);
    vec3 NewNormal;

	//BumpMapNormal = vec3(1,0,1);

    mat3 TBN; 

	TBN = mat3(Tangent, Bitangent, Normal);

    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    return NewNormal;
}

void main(void)
{	
	float frag_dist = length(point_pos_camera.xyz);
	if( frag_dist  < 0.5)
	{
		depth = vec4(1,1,1, 1); 
	}
	else
	{
		depth = vec4(0,0,0, 0);
	}
	vec3 eye_dir;                   // in eye space 
	float atten;                    // attenuation will be 1.0 in case of directional light source
	vec3 light_dir;                 // in eye space
	float z_line = 0.0;
	//int temp_shiny = shiny;
	eye_dir = (-1 * point_pos_camera.xyz);
	  
	if(is_point_light == 1)          // for point light source
	{
		light_dir = trans_light_pos.xyz - point_pos_camera.xyz;  
		float light_dist = length(light_dir);
		//atten = 1.0 / (cons_atten + lin_atten * light_dist + qd_atten * light_dist * light_dist);
		atten = 1;
	}
	else
	{
		light_dir = -1 * light_dir_for_dirLS;   // for directional light source
		atten = 1.0;                      // for directional light source
	}
	
	vec3 ex_nor_l = normalize(ex_nor);
	vec3 eye_dir_l = normalize(eye_dir);	
	vec3 light_dir_l = normalize(light_dir);
	vec3 which_normal = ex_nor_l;

	float always_false = 1.2;

	if(always_false < 0.5){
	
		if(is_invert_narmal == 1)
		{
			ex_nor_l    = -1 * ex_nor_l;
		}

		vec3 half_vector = normalize(light_dir_l + eye_dir_l);
		float diffuse = max(dot(ex_nor_l, light_dir_l), 0.0) ;
		float specular = pow(max(dot( ex_nor_l, half_vector), 0.0), 100);
		vec3 spec_light_col =  vec3(0.0,1.0,0) * specular ;
		vec3 diff_light_col =  vec3(1.0,0,0) * diffuse;
		
		if(is_diffuse == 0)
		{
			diff_light_col  = vec3(0,0,0);
		}

		if(is_specular== 0)
		{
			spec_light_col  = vec3(0,0,0);
		}
						
		vec3 rgb =  min((diff_light_col  + spec_light_col), vec3(1.0));
		color = vec4(vec3(rgb), 1);
	}
	else{
			
		if(is_normal_mapping == 1)
		{
			which_normal = cal_perturbed_nor();
		}

		if(is_invert_narmal == 1)
		{
			which_normal = -1 * which_normal;
			ex_nor_l    = -1 * ex_nor_l;
		}

		vec3 half_vector = normalize(light_dir_l + eye_dir_l);
		//float diffuse = max(abs(dot(ex_nor_l, light_dir_l)), 0.0) ;
		//float specular = pow(max(abs(dot( which_normal, half_vector)), 0.0), shiny);
		float diffuse = max(dot(ex_nor_l, light_dir_l), 0.0) ;
		float specular = pow(max(dot( which_normal, half_vector), 0.0), shiny);

		vec3 spec_light_col =  light_col * specular ;
	
		if(is_gloss_mapping == 1)
		{
			if (which_gloss_map == 1) 
			{
				spec_light_col = (vec3(1,1,1) * (texture2D (gloss_map_tmp, ex_uv)).rgb) * spec_light_col;
			}
			else
			{
				spec_light_col = (vec3(1,1,1) * (texture2D (gloss_map, ex_uv)).rgb) * spec_light_col;
			}
		}
			
		vec3 texel_col;
		vec3 texel_col2;
		vec3 texel_final_col;
		
		vec3 mucous_col;
		vec3 mucous_col2;
		vec3 mucous_final_col;
		
		vec3 diff_col;
		
		// z line formation
		float cutoff = 0.5;
		vec2 uv;

		if(is_point_light == 1){
			uv = ex_uv;
		}
		else{
			uv = point_pos_model.xy;
		}
		
		if( point_pos_model.z > cutoff)
		{
			if (which_texture == 1) 
			{   
				texel_col = (texture2D (texture_tmp, uv)).rgb;
				texel_col2 = (texture2D (texture_tmp, ex_bound_uv_texture1)).rgb;
			}
			else
			{
				texel_col = (texture2D (texture, uv)).rgb;
				texel_col2 = (texture2D (texture, ex_bound_uv_texture1)).rgb;
			}
		}
		else if(point_pos_model.z < (-1.0) * cutoff)
		{
			if (which_texture == 1) 
			{   
				texel_col = (texture2D (texture, uv)).rgb;
				texel_col2 = (texture2D (texture, ex_bound_uv_texture1)).rgb;
			}
			else
			{
				texel_col = (texture2D (texture_tmp, uv)).rgb;
				texel_col2 = (texture2D (texture_tmp, ex_bound_uv_texture1)).rgb;
			}
		}
		else
		{
			float alpha = (point_pos_model.z + cutoff)/ (2.0 * cutoff) ; 
				
			if (which_texture == 1) 
			{   
				texel_col  =  alpha * (texture2D (texture_tmp, uv)).rgb + (1 - alpha) * (texture2D (texture, uv)).rgb ;
				texel_col2 =  alpha * (texture2D (texture_tmp, ex_bound_uv_texture1)).rgb + (1 - alpha) * (texture2D (texture, ex_bound_uv_texture1)).rgb ;
			}
			else
			{   
				texel_col  =  alpha * (texture2D (texture, uv)).rgb + (1 - alpha) * (texture2D (texture_tmp, uv)).rgb ;
				texel_col2 =  alpha * (texture2D (texture, ex_bound_uv_texture1)).rgb + (1 - alpha) * (texture2D (texture_tmp, ex_bound_uv_texture1)).rgb ;
			}
		}
			
		if (which_mucous == 1) 
		{   
			mucous_col = (texture2D (mucous_tmp, uv)).rgb;
			mucous_col2 = (texture2D (mucous_tmp, ex_bound_uv_texture1)).rgb;
		}
		else
		{
			mucous_col = (texture2D (mucous, uv)).rgb;
			mucous_col2 = (texture2D (mucous, ex_bound_uv_texture1)).rgb;
		}
	
		if(is_partial_blending == 1)
		{
			if(ex_is_partial_patch > 5.0)
			{
				float partial_blending;
				if(ex_partial_blending > 1.0)
				{
					partial_blending = 1.0;
				}
				else
				{
					partial_blending = ex_partial_blending;
				}				
				texel_final_col =  ( partial_blending * texel_col +  (1.0 - partial_blending) * texel_col2);
				mucous_final_col =  ( partial_blending * mucous_col +  (1.0 - partial_blending) * mucous_col2);
			}
			else
			{
				texel_final_col =   texel_col;
				mucous_final_col =  mucous_col;
			}
			
		}
		else
		{
			texel_final_col =   texel_col;
			mucous_final_col =  mucous_col;
		}
	
		if(is_mucous == 1){
			float mucous_blending_factor = ex_is_mucous_present;
			diff_col = diffuse * ( (1 - mucous_blending_factor) * texel_final_col + mucous_blending_factor * mucous_final_col);
		}
		else
		{
			diff_col = diffuse * texel_final_col ;
		}
											
		if(show_patch == 1){
			diff_col =  diffuse * ex_patch_col_texture.rgb;
		}
				
		if(is_diffuse == 0)
		{
			diff_col  = vec3(0,0,0);
		}

		if(is_specular== 0)
		{
			spec_light_col  = vec3(0,0,0);
		}
			
		vec3 rgb =  min(atten * (diff_col + amb_light_col + spec_light_col), vec3(1.0));
		color = vec4(rgb, 1);
	}
	
}