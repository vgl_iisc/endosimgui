//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//		Note  ----  1> directional light source or point light source both cases handled
//		            2> eye is nothing but camera
//					3> Normalize after extracting x,y,z components of a vector, don't normalize directly 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#version 330

attribute vec3 in_pos;  // in obj space
attribute vec3 in_nor;  // in obj space
attribute vec2 in_uv; 
attribute vec3 in_tan_vec;   // tangent vector in obj space 
attribute vec3 in_patch_col_texture;
attribute vec2 in_bound_uv_texture1;
attribute float in_is_partial_patch;
attribute float in_partial_blending;
attribute float in_is_mucous_present;

varying vec2 ex_uv;   
varying vec4 point_pos_camera;              
varying vec3 ex_nor;                    // in eye space     
varying vec3 ex_tan_vec;               // tangent vector in eye space
varying vec3 light_dir_for_dirLS;
varying vec4 trans_light_pos;
varying vec3 point_pos_model;   
varying vec3 ex_patch_col_texture;
varying vec2 ex_bound_uv_texture1;
varying float  ex_is_partial_patch;
varying float ex_partial_blending;
varying float ex_is_mucous_present;

uniform float texture_scalling;
uniform mat4  model_mat;
uniform mat4 normal_mat;
uniform mat4 proj_mat;
uniform vec3 dir_light_dir;   // light direction in obj space.. ??
uniform vec3 light_pos;        // light source position in obj space.. ??
uniform float partial_blending;

void main(void)
{
  ex_bound_uv_texture1 =  in_bound_uv_texture1; 
  ex_is_partial_patch =  in_is_partial_patch; 
  ex_partial_blending = in_partial_blending;
  ex_is_mucous_present = in_is_mucous_present;
  
  if(in_partial_blending > 1.0){
	ex_partial_blending = partial_blending;
  }
 
 ex_uv.x = texture_scalling * in_uv.x;  
 ex_uv.y = texture_scalling * in_uv.y; 
 ex_patch_col_texture = in_patch_col_texture;
 vec4 point_pos = ((model_mat) * vec4(in_pos,1.0));   // homogenous point position in world space 
 point_pos_camera = point_pos;                    // point position in eye space because view matrix is identity matrix
 
  trans_light_pos = (model_mat * vec4(light_pos,1.0)) ; 
  //trans_light_pos = vec4(light_pos,1.0) ; 


 //ex_nor = (model_mat * vec4(in_nor,0.0)).xyz;        // if there is no scaling involved in ModelMatrix and view matrix is just a translation matrix
 //ex_tan_vec = (model_mat * vec4(in_tan_vec,0.0)).xyz;
 //light_dir_for_dirLS = (model_mat * vec4(dir_light_dir,0.0)).xyz ;
 
 //ex_nor = (normal_mat * vec4(in_nor,0.0)).xyz;        // if there is no scaling involved in ModelMatrix and view matrix is just a translation matrix
 //ex_tan_vec = (normal_mat * vec4(in_tan_vec,0.0)).xyz;
 //light_dir_for_dirLS = (normal_mat * vec4(dir_light_dir,0.0)).xyz ;

  //ex_nor = mat3(model_mat)  *  in_nor;

 ex_nor = ( transpose( inverse(model_mat) )  *  vec4(in_nor,0.0)  ).xyz;
 ex_tan_vec = ( transpose( inverse(model_mat) )  *  vec4(in_tan_vec,0.0)  ).xyz;
 light_dir_for_dirLS = ( transpose( inverse(model_mat) )  *  vec4(dir_light_dir,0.0)  ).xyz;

 gl_Position = proj_mat * point_pos_camera;
 point_pos_model = in_pos;
}



