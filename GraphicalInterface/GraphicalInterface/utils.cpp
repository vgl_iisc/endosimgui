/*
File : utils.cpp
Author : Tarun Bansal
Description : Basic utility functions
*/

#include "Utils.h"

#define INVALID -1000

using namespace std;

const mat IDENTITY_MATRIX = { {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	} };

float cot(float ang)
{
	return (float)(1.0 / tan(ang));
}

float deg_to_rad(float deg)
{
	return deg * (float)(PI / 180);
}

float rad_to_deg(float rad)
{
	return rad * (float)(180 / PI);
}

mat mult_mat(const mat* m1, const mat* m2)
{
	mat out = IDENTITY_MATRIX;
	unsigned int row, column, row_offset;

	for (row = 0, row_offset = row * 4; row < 4; ++row, row_offset = row * 4)
		for (column = 0; column < 4; ++column)
			out.m[row_offset + column] =
			(m1->m[row_offset + 0] * m2->m[column + 0]) +
			(m1->m[row_offset + 1] * m2->m[column + 4]) +
			(m1->m[row_offset + 2] * m2->m[column + 8]) +
			(m1->m[row_offset + 3] * m2->m[column + 12]);

	return out;
}

void mat_dump(const mat * m){
	int idx = 0;

	fprintf(stderr, "\n");
	fprintf(stderr, "%3f %3f %3f %3f\n ", m->m[idx], m->m[idx + 4], m->m[idx + 8], m->m[idx + 12]);
	idx = 1;
	fprintf(stderr, "%3f %3f %3f %3f \n", m->m[idx], m->m[idx + 4], m->m[idx + 8], m->m[idx + 12]);
	idx = 2;
	fprintf(stderr, "%3f %3f %3f %3f \n", m->m[idx], m->m[idx + 4], m->m[idx + 8], m->m[idx + 12]);
	idx = 3;
	fprintf(stderr, "%3f %3f %3f %3f \n", m->m[idx], m->m[idx + 4], m->m[idx + 8], m->m[idx + 12]);



	fprintf(stderr, "\n");
}

void scl(mat* m, float x, float y, float z)
{
	mat scale = IDENTITY_MATRIX;

	scale.m[0] = x;
	scale.m[5] = y;
	scale.m[10] = z;

	memcpy(m->m, mult_mat(m, &scale).m, sizeof(m->m));
}

void trans(mat* m, float x, float y, float z)
{
	mat translation = IDENTITY_MATRIX;

	translation.m[12] = x;
	translation.m[13] = y;
	translation.m[14] = z;

	memcpy(m->m, mult_mat(m, &translation).m, sizeof(m->m));
}

void rot_x(mat* m, float angle)
{
	mat rotation = IDENTITY_MATRIX;
	float sine = (float)sin(angle);
	float cosine = (float)cos(angle);

	rotation.m[5] = cosine;
	rotation.m[6] = sine;
	rotation.m[9] = -sine;
	rotation.m[10] = cosine;

	memcpy(m->m, mult_mat(m, &rotation).m, sizeof(m->m));
}

void rot_y(mat* m, float angle)
{
	mat rotation = IDENTITY_MATRIX;
	float sine = (float)sin(angle);
	float cosine = (float)cos(angle);

	rotation.m[0] = cosine;
	rotation.m[8] = sine;
	rotation.m[2] = -sine;
	rotation.m[10] = cosine;

	memcpy(m->m, mult_mat(m, &rotation).m, sizeof(m->m));
}

void rot_z(mat* m, float angle)
{
	mat rotation = IDENTITY_MATRIX;
	float sine = (float)sin(angle);
	float cosine = (float)cos(angle);

	rotation.m[0] = cosine;
	rotation.m[1] = sine;
	rotation.m[4] = -sine;
	rotation.m[5] = cosine;

	memcpy(m->m, mult_mat(m, &rotation).m, sizeof(m->m));
}

mat proj(float fovy, float ar, float np, float fp)
{
	mat out = { { 0 } };

	const float
		y_scale = cot(deg_to_rad(fovy / 2)),
		x_scale = y_scale / ar,
		frustum_length = fp - np;

	out.m[0] = x_scale;
	out.m[5] = y_scale;
	out.m[10] = -((fp + np) / frustum_length);
	out.m[11] = -1;
	out.m[14] = -((2 * np* fp) / frustum_length);

	return out;
}

void doNormalize(GLfloat* vec1, int d)
{
	GLfloat len_sq = 0, len = 0;
	for (int i = 0; i<d; i++)
	{
		len_sq += vec1[i] * vec1[i];
	}
	len = sqrt(len_sq);

	if (len > .0000001)
	{
		for (int i = 0; i<d; i++)
		{
			vec1[i] = vec1[i] / len;
		}
	}
}

void calCrossProd(GLfloat* result, GLfloat* vec1, GLfloat* vec2)
{
	result[0] = vec1[1] * vec2[2] - vec1[2] * vec2[1];
	result[1] = vec1[2] * vec2[0] - vec1[0] * vec2[2];
	result[2] = vec1[0] * vec2[1] - vec1[1] * vec2[0];
}

float calDotProd(GLfloat* vec1, GLfloat* vec2){

	return vec1[0] * vec2[0] + vec1[1] * vec2[1] + vec1[2] * vec2[2];
}

void glhLookAtf2(mat *matrix, float *eyePosition3D, float *center3D, float *upVector3D)
{
	float forward[3], side[3], up[3];
	mat matrix2, resultMatrix;

	up[0] = upVector3D[0];
	up[1] = upVector3D[1];
	up[2] = upVector3D[2];

	//------------------
	forward[0] = center3D[0] - eyePosition3D[0];
	forward[1] = center3D[1] - eyePosition3D[1];
	forward[2] = center3D[2] - eyePosition3D[2];

	doNormalize(forward, 3);
	doNormalize(up, 3);

	calCrossProd(side, forward, up);
	doNormalize(side, 3);


	//------------------
	matrix2.m[0] = side[0];
	matrix2.m[1] = side[1];
	matrix2.m[2] = side[2];
	matrix2.m[3] = 0.0;
	//------------------
	matrix2.m[4] = up[0];
	matrix2.m[5] = up[1];
	matrix2.m[6] = up[2];
	matrix2.m[7] = 0.0;
	//------------------
	matrix2.m[8] = -forward[0];
	matrix2.m[9] = -forward[1];
	matrix2.m[10] = -forward[2];
	matrix2.m[11] = 0.0;
	//------------------
	matrix2.m[12] = matrix2.m[13] = matrix2.m[14] = 0.0;
	matrix2.m[15] = 1.0;
	//------------------
	resultMatrix = mult_mat(matrix, &matrix2);
	trans(&resultMatrix, -eyePosition3D[0], -eyePosition3D[1], -eyePosition3D[2]);
	//------------------

	//memcpy(m->m, mult_mat(m, &rotation).m, sizeof(m->m));

	memcpy(matrix, resultMatrix.m, 16 * sizeof(float));
}

void exit_on_gl_error(const char* err_msg)
{
	const GLenum ErrorValue = glGetError();

	if (ErrorValue != GL_NO_ERROR)
	{
		fprintf(stderr, "%s: %s\n", err_msg, gluErrorString(ErrorValue));
		getchar();
		exit(EXIT_FAILURE);
	}
}

bool compiledStatus(GLint shaderID){
	GLint compiled = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
	if (compiled) {
		return true;
	}
	else {
		GLint logLength;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
		char* msgBuffer = new char[logLength];
		glGetShaderInfoLog(shaderID, logLength, NULL, msgBuffer);
		printf("%s\n", msgBuffer);
		delete (msgBuffer);
		return false;
	}
}

GLuint load_shad(const char* filename, GLenum shader_type)
{
	GLuint shader_id = 0;
	FILE* file;
	long file_size = -1;
	char* glsl_source;

	if (NULL != (file = fopen(filename, "rb")) &&
		0 == fseek(file, 0, SEEK_END) &&
		-1 != (file_size = ftell(file)))
	{
		rewind(file);

		if (NULL != (glsl_source = (char*)malloc(file_size + 1)))
		{
			if (file_size == (long)fread(glsl_source, sizeof(char), file_size, file))
			{
				glsl_source[file_size] = '\0';

				if (0 != (shader_id = glCreateShader(shader_type)))
				{
					glShaderSource(shader_id, 1, &glsl_source, NULL);
					glCompileShader(shader_id);

					bool compiledCorrectly = compiledStatus(shader_id);
					if (!compiledCorrectly)
					{
						fprintf(stderr, "Could not compile a shader");
						free(glsl_source);
						fclose(file);
						getchar();
						exit(EXIT_FAILURE);
					}

				}
				else
					fprintf(stderr, "ERROR: Could not create a shader.\n");
			}
			else
				fprintf(stderr, "ERROR: Could not read file %s\n", filename);

			free(glsl_source);
		}
		else
			fprintf(stderr, "ERROR: Could not allocate %i bytes.\n", file_size);

		fclose(file);
	}
	else
	{
		if (NULL != file)
			fclose(file);
		fprintf(stderr, "ERROR: Could not open file %s\n", filename);
	}

	return shader_id;
}

/*
model gen_sinusoidal(){

model mod;
int num_vertices, num_indices, num_normals;
int num_face,grid_size =1000,i;
ver *verts;
GLuint *indx;

num_face = 2*(grid_size-1)*(grid_size-1);
num_vertices = grid_size*grid_size;

num_indices = 3*num_face;
num_normals = num_vertices;


verts = new ver [num_vertices];
indx = new GLuint[num_indices];

i=0;
for(int x=-grid_size/2;x<grid_size/2;x++)
{
for(int z=-grid_size/2;z<grid_size/2;z++)
{
verts[i].pos[0] = x/30.0;
verts[i].pos[1] = sin((float)x/10.0)*sin((float)z/10.0);
verts[i].pos[2] = z/30.0;

verts[i].uv[0] = verts[i].pos[0];
verts[i].uv[1] = verts[i].pos[1];

verts[i].nor[0] = verts[i].nor[1] = verts[i].nor[2] = 0;
i++;
}
}

i=0;
for(int z=0;z<grid_size-1;z++)
{
for(int x=0;x<grid_size-1;x++)
{

indx[3*i +0] = z*grid_size + x;
indx[3*i +1] = z*grid_size + (x+1);
indx[3*i +2] = (z+1)*grid_size + x;

GLfloat nor[3],edge1[3],edge2[3];

for(int k=0;k < 3;k++)
{
edge1[k] = verts[indx[3*i +0]].pos[k]  - verts[indx[3*i +1]].pos[k];
edge2[k]  =verts[indx[3*i +0]].pos[k]  - verts[indx[3*i +2]].pos[k];

}

calCrossProd(nor,edge1,edge2);
doNormalize (nor,3);

for(int k=0;k < 3;k++)
{
verts[indx[3*i + 0]].nor[k]  += nor[k];
verts[indx[3*i + 1]].nor[k]  += nor[k];
verts[indx[3*i + 2]].nor[k]  += nor[k];
}
i++;

indx[3*i +0] = (z+1)*grid_size + x;
indx[3*i +1] = z*grid_size + (x+1);
indx[3*i +2] = (z+1)*grid_size + (x+1);

for(int k=0;k < 3;k++)
{
edge1[k] = verts[indx[3*i +0]].pos[k]  - verts[indx[3*i +1]].pos[k];
edge2[k] = verts[indx[3*i +0]].pos[k]  - verts[indx[3*i +2]].pos[k];

}

calCrossProd(nor,edge1,edge2);
//doNormalize (nor,3);

for(int k=0;k < 3;k++)
{
verts[indx[3*i + 0]].nor[k]  += nor[k];
verts[indx[3*i + 1]].nor[k]  += nor[k];
verts[indx[3*i + 2]].nor[k]  += nor[k];
}
i++;

}
}


for(int i=0;i < num_normals ; i++)
{
float len = sqrt(verts[i].nor[0] * verts[i].nor[0] + verts[i].nor[1] * verts[i].nor[1] + verts[i].nor[2] * verts[i].nor[2]  );
if(len > 0)
{
verts[i].nor[0] = verts[i].nor[0]/len;
verts[i].nor[1] = verts[i].nor[1]/len;
verts[i].nor[2] = verts[i].nor[2]/len;
}

}
mod.indx = indx;
mod.num_triangle = num_face;
mod.num_vert = num_vertices;
mod.vers = verts;
return mod;
}
*/

float min(float a, float b, float c)
{
	if (a < b)
	{
		return (a < c ? a : c);
	}
	else
	{
		return (b < c ? b : c);
	}
}

float max(float a, float b, float c)
{
	if (a > b)
	{
		return (a > c ? a : c);
	}
	else
	{
		return (b > c ? b : c);
	}
}

int max_int(int a, int b, int c){
	if (a > b)
	{
		return (a > c ? a : c);
	}
	else
	{
		return (b > c ? b : c);
	}
}

image loadBMP_custom(const char * imagepath){

	printf("Reading image %s\n", imagepath);

	// Data read from the header of the BMP file
	image img = { NULL, 0, 0 };
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;


	//int east_x, east_y;
	//int north_x, north_y;
	//int west_x, west_y;
	//int south_x, south_y;

	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath, "rb");

	if (!file)
	{
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath);
		getchar();
		return img;
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54){
		printf("Not a correct BMP file\n");
		return img;
	}

	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M'){
		printf("Not a correct BMP file\n");
		return img;
	}

	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0)
	{
		printf("Not a correct BMP file\n");
		return img;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		printf("Not a correct BMP file\n");
		return img;
	}

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

	// Create a buffer
	data = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(data, 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	img.height = height;
	img.width = width;
	img.pixels = data;
	return img;
}

void swap(int *a, int *b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void printArray(int arr[], int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

void randomize(int arr[], int n)
{
	// Use a different seed value so that we don't get same
	// result each time we run this program
	srand(time(NULL));

	// Start from the last element and swap one by one. We don't
	// need to run for the first element that's why i > 0
	for (int i = n - 1; i > 0; i--)
	{
		// Pick a random index from 0 to i
		int j = rand() % (i + 1);

		// Swap arr[i] with the element at random index
		swap(&arr[i], &arr[j]);
	}
}

Camera3d::Camera3d(){
	pos.set(0., 0., 0.);
	eye.set(0., 0., 1.);
	ori.set(0., 1., 0.);
	ang = 45.0;
	cnear = 1.0;
	cfar = 500.0;
	ar = 1.0;
}

Camera3d& Camera3d::operator = (const Camera3d &c){
	pos = c.pos;
	eye = c.eye;
	ori = c.ori;
	ang = c.ang;
	cnear = c.cnear;
	cfar = c.cfar;
	ar = c.ar;
	return *this;
}