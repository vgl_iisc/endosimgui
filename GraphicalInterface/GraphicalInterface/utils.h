/*
File : utils.h
Author : Tarun Bansal
Description : Basic utility functions
*/

#ifndef UTILS_H
#define UTILS_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include<cmath>
#include<ctgmath>
#include <vector>
#include <queue>
#include <iterator>

#include "my_vector.h"
#include <GL/glew.h>
#include <GL/freeglut.h>

using namespace std;

#define PI	3.14159265358

typedef struct edge{
	int start;
	int end;
} edge;

typedef struct mod_edge{

	int u_ver_idx, v_ver_idx, tri_idx;

	mod_edge(int u, int v, int tri){
		tri_idx = tri;
		if (u < v){
			u_ver_idx = u; v_ver_idx = v;
		}
		else{
			u_ver_idx = v; v_ver_idx = u;
		}
	}

	bool operator < (const mod_edge& me) const{
		if (u_ver_idx < me.u_ver_idx)
			return true;
		else if (u_ver_idx == me.u_ver_idx)
			return v_ver_idx < me.v_ver_idx;
		return false;
	}

} mod_edge;


typedef struct ver
{
	Vector3f pos;
	Vector3f nor;
	Vector2f uv_texture;
	Vector3f tangent;
	Vector3f patch_col_texture;
	Vector2f bound_uv_texture1;
	//float bound_uv_texture2[2];
	float is_partial_patch;
	float partial_blending;	
	float is_mucous_present;
} ver;

typedef struct model_triangle
{
	int ver0;
	int ver1;
	int ver2;

}model_triangle;

typedef struct model
{
	ver *vers;
	model_triangle *triangles;
	unsigned num_vert;
	unsigned num_triangle;

} model;

typedef struct graph_node {

	int edge0;
	int edge1;
	int edge2;

} graph_node;

typedef struct graph
{
	model *mod;
	int node_cnt;
	graph_node *nodes;
} graph;

typedef struct cam_path
{
	Vector3f *pts;
	unsigned num_pos;
} cam_path;

typedef double POINTTYPE;

typedef struct {
	POINTTYPE x;
	POINTTYPE y;
	POINTTYPE z;

	void set(int X, int Y, int Z){
		x = X; y = Y; z = Z;
	}
}Point;

class BSpline{
private:
	int _n;        // no of control points are n+1
	int _m;        // no of knots are m+1
	int _d;

public:

	double *kv;   // knot vector
	Point *cp; //control points

	BSpline(){
		_m = _n = _d = 0;
		cp = NULL;
		kv = NULL;
	}

	~BSpline(){
		_m = _n = _d = 0;
		if (cp != NULL){
			delete[] cp;
			cp = NULL;
		}
		if (kv != NULL){
			delete[] kv;
			kv = NULL;
		}
	}

	bool initCurve(int controlPoints, int p){
		if (1 <= p && p <= controlPoints){
			_d = p;
			_n = controlPoints;
			cp = new Point[_n + 1];
			_m = _n + _d + 1;
			kv = new double[_m + 1];
			int span = _m - 2 * _d - 1;
			for (int i = 0; i <= _d; i++){
				kv[i] = 0;
				kv[_m - i] = 1;
			}
			for (int i = 1; i <= span; i++)
				kv[i + _d] = (double)(i) / (double)(span + 1);
			return true;
		}
		return false;
	}

	int getD(){
		return _d;
	}

	int getN(){
		return _n;
	}

	int getM(){
		return _m;
	}

	double N(int i, int p, double u){
		if (0 <= i && 0 <= p&&p <= _d && 0.0 <= u&&u <= 1.0){
			if (p == 0){
				if (kv[i] <= u&&u < kv[i + 1])
					return 1;
				return 0;
			}
			else if (kv[i + p] == kv[i] && kv[i + p + 1] == kv[i + 1])
				return 0;
			else if (kv[i + p] == kv[i])
				return N(i + 1, p - 1, u)*(kv[i + p + 1] - u) / (kv[i + p + 1] - kv[i + 1]);
			else if (kv[i + p + 1] == kv[i + 1])
				return N(i, p - 1, u)*(u - kv[i]) / (kv[i + p] - kv[i]);
			return N(i, p - 1, u)*(u - kv[i]) / (kv[i + p] - kv[i]) + N(i + 1, p - 1, u)*(kv[i + p + 1] - u) / (kv[i + p + 1] - kv[i + 1]);
		}
		return 0.0;
	}

	Point C(double u){
		Point r = { 0, 0, 0 };
		if (0.0 <= u&&u <= 1.0){
			for (int i = 0; i <= _n; i++){
				double nip = N(i, _d, u);
				r.x += nip*cp[i].x;
				r.y += nip*cp[i].y;
				r.z += nip*cp[i].z;
			}
		}
		return r;
	}

	Point derivative(double u){
		Point r = { 0, 0, 0 };
		if (0.0 <= u&&u <= 1.0){
			for (int i = 0; i<_n; i++){
				double nip = N(i + 1, _d - 1, u);
				double m = 1;
				m = (double)(_d) / (double)(kv[i + _d + 1] - kv[i + 1]);
				r.x += nip*m*(cp[i + 1].x - cp[i].x);
				r.y += nip*m*(cp[i + 1].y - cp[i].y);
				r.z += nip*m*(cp[i + 1].z - cp[i].z);
			}
		}
		return r;
	}

	void lsApprox(int h, Point *data){ //h+1 data points
		double *t = new double[h + 1];
		for (int i = 0; i <= h; i++)
			t[i] = (double)(i) / (double)(h);
		cp[0] = data[0];
		cp[_n] = data[h];
		Point *Qk = new Point[h + 1];
		for (int k = 1; k<h; k++){
			double m1 = N(0, _d, t[k]);
			double m2 = N(_n, _d, t[k]);
			Qk[k].x = data[k].x - m1*data[0].x - m2*data[h].x;
			Qk[k].y = data[k].y - m1*data[0].y - m2*data[h].y;
			Qk[k].z = data[k].z - m1*data[0].z - m2*data[h].z;
		}
		double *Q = new double[(_n - 1) * 3]; //3-D
		for (int i = 1; i<_n; i++){
			double sx = 0, sy = 0, sz = 0;
			for (int k = 1; k<h; k++){
				double m = N(i, _d, t[k]);
				sx += m*Qk[k].x;
				sy += m*Qk[k].y;
				sz += m*Qk[k].z;
			}
			Q[3 * (i - 1) + 0] = sx;
			Q[3 * (i - 1) + 1] = sy;
			Q[3 * (i - 1) + 2] = sz;
		}
		double *aN = new double[(h - 1)*(_n - 1)];
		for (int k = 1, idx = 0; k<h; k++)
			for (int i = 1; i<_n; i++, idx++)
				aN[idx] = N(i, _d, t[k]);
		double *NTN = new double[(_n - 1)*(_n - 1)];
		for (int j = 0, idx = 0; j<_n - 1; j++)
			for (int i = 0; i<_n - 1; i++, idx++){
				double s = 0;
				for (int k = 0; k<h - 1; k++)
					s += aN[k*(_n - 1) + j] * aN[k*(_n - 1) + i];
				NTN[idx] = s;
			}
		double *N_1 = new double[(_n - 1)*(_n - 1)];
		for (int i = 0, k = 0; i<_n - 1; i++)
			for (int j = 0; j<_n - 1; j++, k++)
				if (i == j)
					N_1[k] = 1;
				else
					N_1[k] = 0;
		for (int i = 0; i<_n - 1; i++){
			double d = NTN[i*(_n - 1) + i];
			for (int j = 0; j<_n - 1; j++){
				if (i == j)
					continue;
				double r = NTN[j*(_n - 1) + i] / d;
				for (int k = 0, lj = j*(_n - 1), li = i*(_n - 1); k<_n - 1; k++, li++, lj++){
					NTN[lj] -= r*NTN[li];
					if ((NTN[lj]>0 && NTN[lj]<1.0e-14) || (NTN[lj]<0 && NTN[lj]>-1.0e-14))
						NTN[lj] = 0;
					N_1[lj] -= r*N_1[li];
					if ((N_1[lj]>0 && N_1[lj]<1.0e-14) || (N_1[lj]<0 && N_1[lj]>-1.0e-14))
						N_1[lj] = 0;
				}
			}
		}
		for (int i = 0; i<_n - 1; i++){
			double d = NTN[i*(_n - 1) + i];
			for (int k = 0, li = i*(_n - 1); k<_n - 1; k++, li++)
				N_1[li] /= d;
		}
		for (int i = 0; i<_n - 1; i++){
			double sx = 0, sy = 0, sz = 0;
			for (int j = 0; j<_n - 1; j++){
				sx += N_1[i*(_n - 1) + j] * Q[j * 3 + 0];
				sy += N_1[i*(_n - 1) + j] * Q[j * 3 + 1];
				sz += N_1[i*(_n - 1) + j] * Q[j * 3 + 2];
			}
			cp[i + 1].set(sx, sy, sz);
		}
		delete[] N_1;
		delete[] NTN;
		delete[] aN;
		delete[] Q;
		delete[] Qk;
		delete[] t;
	}
};

typedef struct image
{
	unsigned char *pixels;
	unsigned width;
	unsigned height;
} image;

typedef struct mat
{
	float m[16];
} mat;

extern const mat IDENTITY_MATRIX;

float cot(float ang);
float deg_to_rad(float deg);
float rad_to_deg(float rad);

mat mult_mat(const mat* m1, const mat* m2);
void rot_x(mat* m, float ang);
void rot_y(mat* m, float ang);
void rot_z(mat* m, float ang);
void scl(mat* m, float x, float y, float z);
void trans(mat* m, float x, float y, float z);

void glhLookAtf2(mat *matrix, float *eyePosition3D, float *center3D, float *upVector3D);

mat proj( float fovy, float ar, float np, float fp);
void exit_on_gl_error(const char* err_msg);
GLuint load_shad(const char* filename, GLenum shader_type);
bool compiledStatus(GLint shaderID);
void mat_dump(const mat * m);
image loadBMP_custom(const char * imagepath);
float min(float a, float b, float c);
float max(float a, float b, float c);
int max_int(int a, int b, int c);

// function for generating a random permutaion
void randomize(int arr[], int n);
void swap(int *a, int *b);
void printArray(int arr[], int n);
void myinit(int, char *[]);
void init_window(int, char *[]);
void resize(int, int);
void render();
void timer_func(int);
void idle(void);
void process_model(void);
void destroy_model(void);
void draw_model(void);
void mouseWheel(int button, int dir, int x, int y);
void keyboard(unsigned char, int, int);
void init_uniforms(void);
void load_textures(void);
void get_uniforms();
void populate_buffer_for_model();
void gen_ver_array();
void handle_shader_program();
void define_callbacks();
void create_and_link_shader();
void populate_buffer_for_model();
void populate_buffer_for_circular_mask();
void process_texture(const char * imagepath, int texture_unit, GLuint *texture_id, GLuint uloc);
void mousepass(int x, int y);
void mousemotionfunc(int a, int b);
void mousefunc(int button, int state, int a, int b);
void processTrackBall(const Vector2f& old, const Vector2f& now);
void draw_eye_and_light();
void draw_normals();
void draw_pipe();
void reset_quat();
void cleanupQuadric(void);

class Camera3d{
public:
	Vector3f pos;	// position of the camera
	Vector3f eye;	// where they are pointing
	Vector3f ori;	// orientation
	float ang;		// angle;
	float cnear;     // camera near
	float cfar;      // camera far
	float ar;		// aspect ratio
	Camera3d();
	Camera3d& operator = (const Camera3d &c);
};


#endif